package no.uib.info310.norgerundt.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class that finds the name of the program host from the episode description.
 * There are too many corner cases to do it reliably for every episode,
 * but we get most of them.
 */
public class ProgramHostExtractor {
    private static ProgramHostExtractor ourInstance = new ProgramHostExtractor();

    public static ProgramHostExtractor getInstance() {
        return ourInstance;
    }

    private ProgramHostExtractor() {
    }

    /**
     * Get multiple host names from the description if there are more than one.
     *
     * @param episodeDescription the description to get the hosts from
     * @return the host names
     */
    public List<String> extractMultipleHosts(String episodeDescription){
        Pattern pattern = Pattern.compile("[Pp]rogramledere:?( er)? ((([A-ZÆØÅ][a-zæøå.]*([ .-]|))* ?(og|,)?)*)");
        Matcher matcher = pattern.matcher(episodeDescription);

        List<String> hosts = null;
        if (matcher.find()){
            String[] hostNames = matcher.group(2).split("( og |, )");

            hosts = new ArrayList<>();
            for (String name : hostNames) {
                hosts.add(sanitizeHostName(name));
            }
        }
        return hosts;
    }

    /**
     * Extract the host name from the description of an episode
     *
     * @param episodeDescription the description the get the host from
     * @return the host name
     */
    public String extractHost(String episodeDescription){
        Pattern pattern = Pattern.compile("[Pp]rogram(leder?|leiar)[,.:]? +(([A-ZÆØÅ][a-zæøå.]*([ .-]|))*)");
        Matcher matcher = pattern.matcher(episodeDescription);

        if (matcher.find()){
            return sanitizeHostName(matcher.group(2));
        } else {
            System.err.println("HostExtractor: Failed to extract host from " + episodeDescription);
        }
        return null;
    }

    /**
     * Sanitizes the host names to make them presentable.
     *
     * @param name the host name to sanitize
     * @return the sanitized host name
     */
    public String sanitizeHostName(String name) {
        Pattern pattern = Pattern.compile("((([A-ZÆØÅ][a-zæøå]*)( |. (?!.))?)+)");
        Matcher matcher = pattern.matcher(name);
        if (matcher.find())
            name = matcher.group(1).trim();
        else {
            System.err.println("HostExtractor: Failed to sanitize name \"" + name + "\"");
            return null;
        }
        return name;
    }
}
