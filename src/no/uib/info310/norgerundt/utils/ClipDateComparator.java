package no.uib.info310.norgerundt.utils;

import no.uib.info310.norgerundt.data.Clip;

import java.util.Comparator;

/**
 * Class to compare the dates of clips in an episode,
 * used to make sure the clips are in the right order.
 */
public class ClipDateComparator implements Comparator<Clip> {

    @Override
    public int compare(Clip o1, Clip o2) {
        int dateCompResult = o1.getDato().compareTo(o2.getDato());
        //If the clips aired on the same date, compare position in episode
        if (dateCompResult == 0){
            if (o1.getTimestampInSeconds() > o2.getTimestampInSeconds())
                return 1;
            else if (o2.getTimestampInSeconds() > o1.getTimestampInSeconds())
                return -1;
            else
                return 0;
        }
        return dateCompResult;
    }
}
