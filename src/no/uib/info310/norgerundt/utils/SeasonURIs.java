package no.uib.info310.norgerundt.utils;

/**
 * A class the get URIs for seasons of Norge Rundt
 */
public class SeasonURIs {

    /**
     * Utility method for retrieving the URI of a specific season of Norge Rundt
     * based on the year it aired
     *
     * @return A URI pointing to the location of this season on tv.nrk.no
     */
    public static String getSeasonURI(int year) {
        String baseURI = "https://tv.nrk.no/program/Episodes/norge-rundt/";
        switch (year) {
            case 2016:
                return baseURI + 73683;
            case 2015:
                return baseURI + 54344;
            case 2014:
                return baseURI + 37928;
            case 2008:
                return baseURI + 42982;
            case 2007:
                return baseURI + 66543;
            case 2006:
                return baseURI + 61102;
            case 2005:
                return baseURI + 66502;
            case 2004:
                return baseURI + 52462;
            case 2003:
                return baseURI + 58122;
            case 2002:
                return baseURI + 33576;
            case 1982:
                return baseURI + 36649;
            case 1976:
                return baseURI + 50522;
            default:
                return baseURI + (22399 + year - 1977);
        }
    }
}
