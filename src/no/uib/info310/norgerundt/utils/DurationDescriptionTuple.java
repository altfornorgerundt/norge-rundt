package no.uib.info310.norgerundt.utils;

/**
 * Class representing a tuple of episode description and duration.
 * This information is scraped from NRK and stored to avoid having
 * to do it each time the application is started.
 */
public class DurationDescriptionTuple {

    private String description;
    private int durationInSeconds;

    public DurationDescriptionTuple(){
        description = "";
        durationInSeconds = -1;
    }

    /*
     * Setters and getters
     */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDurationInSeconds() {
        return durationInSeconds;
    }

    public void setDurationInSeconds(int durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }
}
