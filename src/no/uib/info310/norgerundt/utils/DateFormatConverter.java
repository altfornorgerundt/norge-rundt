package no.uib.info310.norgerundt.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Class to convert dates in the yyyy-mm-dd
 * format to something more readable.
 */
public abstract class DateFormatConverter {

    public static String convertDate(String inputDate){

        Locale locale = new Locale("no", "NO");
        LocalDate newDate = LocalDate.parse(inputDate);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d. MMMM yyyy");
        newDate.format(formatter.withLocale(locale));

        return newDate.format(formatter);
    }
}
