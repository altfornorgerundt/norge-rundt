package no.uib.info310.norgerundt.utils;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


public class TupleMapBuilder {

    /**
     * Utility method used to build a label + url tuple to resource map based on a
     * label property such as skos:prefLabel or schema:name
     */
    public static Map<UrlTitleTuple, Resource> buildTupleResourceMap(List<Resource> clipResources, Property titleProperty) {
        return clipResources.stream()
                .filter(clipRes -> clipRes.hasProperty(titleProperty))
                .collect(Collectors.toMap(
                        key -> new UrlTitleTuple(key.getURI(), key.getProperty(titleProperty).getString()),
                        Function.identity()));
    }
}
