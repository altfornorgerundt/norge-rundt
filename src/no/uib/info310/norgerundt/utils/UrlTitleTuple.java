package no.uib.info310.norgerundt.utils;

/**
 * Utility class used to couple item labels/names
 * to their URIs to enable mapping for use with the
 * searchField in GUIController
 */
public class UrlTitleTuple {

    private String url;
    private String data;

    /**
     * Constructs a tuple for this item by doing the reverse
     * of this class' toString() method
     * @param dataAndUrl A string of the item's name and URI
     *                   separated by a comma followed by whitespace
     */
    public UrlTitleTuple(String dataAndUrl){
        String[] fields = dataAndUrl.split(", ");
        data = fields[0];
        url = fields[1];
    }

    public UrlTitleTuple(String url, String data) {
        this.url = url;
        this.data = data;
    }


    public String getUrl() {
        return url;
    }

    public String getData() {
        return data;
    }

    /**
     * Returns a unique string composed of the item's
     * URI and it's name/label for use in mapping
     * Reversible by constructing a new tuple with
     * new UrlTitleTuple(this.toString())
     * @return A string of the item's name/label followed by
     * a comma and whitespace and then its URI
     */
    @Override
    public String toString(){
        return data + ", " + url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UrlTitleTuple that = (UrlTitleTuple) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return data != null ? data.equals(that.data) : that.data == null;
    }


    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }
}
