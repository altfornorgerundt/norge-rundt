package no.uib.info310.norgerundt.utils;

import org.apache.jena.rdf.model.Resource;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class containing various URL management methods
 */
public abstract class UrlManager {

	/**
	 * Removes autostart parameter from any URL that has one
	 * @param url the URL to remove it from
	 * @return the URL without autostart parameter
	 */
	public static String removeAutostartFromURL(String url){
		return url.replaceAll("(\\?autostart=true)", "");
	}

	/**
	 * CHecks is a URL has a timestamp, adds one at 0 seconds if it doesn't
	 * @param url the URL the get the ttimestamp from
	 * @return the URL with a timestamp
	 */
	public static String addTimestampToURL(String url){

		String pattern = "#t=(\\d+[hsm]){1,3}";
		Pattern timestamp = Pattern.compile(pattern);
		Matcher m = timestamp.matcher(url);

		if (m.find()){
			return url;
		}
		if (url.charAt(url.length() - 1) == '#')
		    url = url.substring(0, url.length() - 1);
		return url  + "#t=0s";
	}
	
	/**
	 * Takes a URL, finds a date and returns it.
	 * @param url the input URL
	 * @return the date from the URL
	 */
	public static LocalDate getDateFromURL(String url) {
		
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String pattern = "\\d{2}-\\d{2}-\\d{4}";
		Pattern date = Pattern.compile(pattern);
		Matcher m = date.matcher(url);

		if (m.find()) {
				return LocalDate.parse(m.group(), format);
		}
		throw new RuntimeException("Failed to parse date from " + url);
	}

	/**
	 * Takes the web address of a Clip and strips it of its timestamp
	 * so it points to the Episode as a whole instead
	 */
	public static String stripTimestamp(String innslag_nettadresse) {
		//https://tv.nrk.no/serie/norge-rundt/DVNR04001416/08-04-2016
		return innslag_nettadresse.substring(0, 48);
	}

	/**
	 * Removed the date from a URL based on character position
	 *
	 * @param url the URL to remove the date from
	 * @return the URL without the date
	 */
	public static String stripDateFromUrl(String url){
		return url.substring(0, 48) + url.substring(59);
	}

	/**
	 * Opens a URL in a web browser
	 *
	 * @param url the url to open
	 */
	public static void openUrlInBrowser(String url) {
		try {
			Desktop.getDesktop().browse(new URI(url));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}
}
