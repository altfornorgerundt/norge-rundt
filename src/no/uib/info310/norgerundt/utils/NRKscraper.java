package no.uib.info310.norgerundt.utils;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class used to scrape NRKs web pages for each episode in order to
 * retrieve its duration
 */
public class NRKscraper {

    /*
     * Singleton
     */
    private static NRKscraper instance;

    public static NRKscraper getInstance() {
        if (instance == null)
            instance = new NRKscraper();
        return instance;
    }

    private NRKscraper() {}

    /**
     * Fetches the duration of the episode that the input url corresponds to
     *
     * @return The duration of the episode in seconds
     */
    public DurationDescriptionTuple scrapeEpisodeInfo(String episodeURL) {
        DurationDescriptionTuple tuple = new DurationDescriptionTuple();

        try {
            URL episodeUrl = new URL(episodeURL);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(episodeUrl.openStream()));

            Pattern durationPattern = Pattern.compile("\"duration\":(\\d*)");
            Pattern descriptionPattern = Pattern.compile("description\" content=\"([^\\/]*?(\"|$))");
            boolean descriptionFound = false;

            String line;
            while ((line = reader.readLine()) != null) {
                Matcher matcher;
                if (!descriptionFound) {
                    matcher = descriptionPattern.matcher(line);
                    if (matcher.find()) {
                        String description = matcher.group(1);
                        //NRK has a line break in their description, thanks guys
                        if (!description.endsWith("\""))
                            description = handleLinebreaksInDesc(description, reader);
                        description = description.substring(0, description.length() -1);
                        description = StringEscapeUtils.unescapeHtml4(description);
                        tuple.setDescription(description);
                        descriptionFound = true;
                    }
                } else {
                    matcher = durationPattern.matcher(line);
                    if (matcher.find()) {
                        tuple.setDurationInSeconds(Integer.parseInt(matcher.group(1)));
                        return tuple;
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Failed to find description or duration for episode " + episodeURL);
        return tuple;
    }

    /**
     * Handles cases where there are line breaks in the description.
     */
    private String handleLinebreaksInDesc(String description, BufferedReader reader) throws IOException {
        String line;
        Pattern descriptionEndPattern = Pattern.compile("(.*\")");
        while ((line = reader.readLine()) != null){
            Matcher descriptionEndMatcher = descriptionEndPattern.matcher(line);
            if (!descriptionEndMatcher.find())
                description += ("\n" + line);
            else {
                description += ("\n" + descriptionEndMatcher.group(1));
                break;
            }
        }
        return description;
    }
}
