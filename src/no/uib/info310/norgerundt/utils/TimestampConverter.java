package no.uib.info310.norgerundt.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to convert a timestamp to seconds, and convert seconds to a timestamp
 */
public abstract class TimestampConverter {

    /**
     * Takes a timestamp in seconds and converts it to hh:mm:ss
     * @param timeInSeconds the input time in seconds to convert
     * @return returns the new timestamp
     */
    public static String convertSecondsToTimestamp(int timeInSeconds){

        int hours = timeInSeconds / 3600;
        int minutes = (timeInSeconds % 3600) /60;
        int seconds = timeInSeconds % 60;

        String newTimestamp = "";

        if (hours != 0){newTimestamp += hours + "h";}
        if (minutes != 0){newTimestamp += minutes + "m";}
        if (seconds != 0){newTimestamp += seconds + "s";}

        return newTimestamp;
    }

    /**
     * Takes a timestamp from a URL and converts it to seconds
     * @param url the URL to take the timestamp from
     * @return returns the time in seconds
     */
    public static int convertTimestampToSeconds(String url){

        int timestampInSeconds = 0;
        String time = "";
        String timestampPattern = "#t=(\\d+[hsm]){1,3}";
        Pattern timestamp = Pattern.compile(timestampPattern);
        Matcher timestampMatcher = timestamp.matcher(url);

        if (timestampMatcher.find()){

            Pattern timePattern = Pattern.compile("(#t=)(\\d+[h])?(\\d+[m])?(\\d+[s])?");
            Matcher m = timePattern.matcher(url);
            m.find();

            //Possibly redundant initialization, getting warnings since if's won't necessarily initialize variable
            int hours = 0; int minutes = 0; int seconds = 0;

            if(m.group(2) != null){
                hours = Integer.parseInt(m.group(2).substring(0,m.group(2).length()-1));
                timestampInSeconds += hours*3600;
            }
            if (m.group(3) != null){
                minutes = Integer.parseInt(m.group(3).substring(0,m.group(3).length()-1));
                timestampInSeconds += minutes*60;
            }
            if (m.group(4)!= null){
                seconds = Integer.parseInt(m.group(4).substring(0,m.group(4).length()-1));
                timestampInSeconds += seconds;
            }

            return timestampInSeconds;
        }
            else{
            return 0;
        }
    }
}
