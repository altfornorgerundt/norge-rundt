package no.uib.info310.norgerundt.utils;

import no.uib.info310.norgerundt.data.Clip;
import no.uib.info310.norgerundt.semantics.QueryBuilder;
import no.uib.info310.norgerundt.semantics.SubjectLifter;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.tdb.TDBFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static no.uib.info310.norgerundt.utils.PrefixHolder.LOCAL_BASE;
import static no.uib.info310.norgerundt.utils.PrefixHolder.RDF_BASE;
import static no.uib.info310.norgerundt.utils.PrefixHolder.SKOS_BASE;

/**
 * Class to create a taxonomy based on several main themes,
 * each of which have many subthemes.
 */
public class TaxonomyPopulator {

    public static final String THEME_CACHE_LOCATION = "cache/themeCache";
    public static final String DEFAULT_LANG_TAG = "nb";
    private Property prefLabelProperty;
    private Property hasTopConceptProp;
    private Property narrowerProperty;
    private Property typeProperty;
    private Resource conceptTypeResource;
    private Model model;
    private Map<String, Set<String>> mainSubTermMap;
    private static TaxonomyPopulator instance;

    private TaxonomyPopulator(){}

    /*
     * Singleton
     */
    public static TaxonomyPopulator getInstance(){
        if (instance == null) {
            instance = new TaxonomyPopulator();
        }
        return instance;
    }

    /**
     * To be used with buildThemeMap()
     * Prints a list of all themes with sub themes divided by which
     * main theme they belong to
     */
    public void writeTaxonomyTerms(List<Clip> clips) {
        initializeModel();

        model.begin();
        model.setNsPrefix("skos", SKOS_BASE);

        initializeProperties();

        mainSubTermMap = buildThemeMap(clips);

        Resource conceptScheme = model.createResource(LOCAL_BASE + "themeScheme");
        conceptScheme.addProperty(typeProperty, model.createResource(SKOS_BASE + "ConceptScheme"));

        liftMainThemes(conceptScheme);
        model.commit();
    }

    /**
     * Initializes the model
     */
    private void initializeModel() {
        Dataset dataset = TDBFactory.createDataset(THEME_CACHE_LOCATION);
        model = dataset.getDefaultModel();

        conceptTypeResource = model.createResource(SKOS_BASE + "Concept");
    }

    /**
     * Initializes the properties for the model
     */
    private void initializeProperties() {
        prefLabelProperty = model.createProperty(SKOS_BASE + "prefLabel");
        hasTopConceptProp = model.createProperty(SKOS_BASE + "hasTopConcept");
        narrowerProperty = model.createProperty(SKOS_BASE + "narrower");
        typeProperty = model.createProperty(RDF_BASE + "type");
    }

    /**
     * Returns a map of main themes mapped to sets of sub themes, based on
     * the themes found in the provided list of clips
     */
    public Map<String, Set<String>> buildThemeMap(List<Clip> clips) {
        Map<String, List<Clip>> hovedtemaMap = clips.stream()
                .collect(Collectors.groupingBy(Clip::getInnslag_hovedtema));

        Map<String, Set<String>> hovedundertemaMap = new HashMap<>();
        for (String hovedtema : hovedtemaMap.keySet()) {
            List<Clip> clipList = hovedtemaMap.get(hovedtema);
            Set<String> undertemaer = clipList.stream()
                    .map(Clip::getUndertema)
                    .flatMap(List::stream)
                    .collect(Collectors.toSet());
            hovedundertemaMap.put(hovedtema, undertemaer);
        }
        return hovedundertemaMap;
    }

    /**
     * Lifting the main categories of the data set,
     * each of which have many subthemes.
     *
     * @param conceptScheme the conceptScheme to add the main themes to
     */
    private void liftMainThemes(Resource conceptScheme) {
        for (String mainTheme : mainSubTermMap.keySet()) {
            //Ignore ukategoriserbart and any subthemes
            if (mainTheme.equals("Ukategoriserbart"))
                continue;

            String escapedTheme = StringEscapeUtils.escapeHtml4(mainTheme).replaceAll(" ", "_");
            Resource mainThemeResource = model.createResource(LOCAL_BASE + escapedTheme);
            mainThemeResource.addProperty(prefLabelProperty, mainTheme);
            mainThemeResource.addProperty(typeProperty, conceptTypeResource);
            conceptScheme.addProperty(hasTopConceptProp, mainThemeResource);

            liftSubThemes(mainTheme, mainThemeResource);
        }
    }

    /**
     * Lifting the subthemes from each main theme
     *
     * @param mainTheme the main themes to get subthemes from
     * @param mainThemeResource the resource to add the lifted subthemes to
     */
    private void liftSubThemes(String mainTheme, Resource mainThemeResource) {
        for (String subTheme : mainSubTermMap.get(mainTheme)) {
            subTheme = subTheme.toLowerCase();
            SubjectLifter lifter = SubjectLifter.getInstance();
            Resource subThemeResource = lifter.getWordResource(subTheme);
            Literal prefLabel;

            if (subThemeResource != null) {
                prefLabel = lifter.getLexicalForm(subThemeResource);
                model.add(subThemeResource, typeProperty, conceptTypeResource);
                model.add(subThemeResource, prefLabelProperty, prefLabel);
            } else {
                String escapedSubTheme = StringEscapeUtils.escapeHtml4(subTheme).replaceAll(" ", "_");
                subThemeResource = model.createResource(LOCAL_BASE + escapedSubTheme);
                prefLabel = model.createLiteral(subTheme, DEFAULT_LANG_TAG);

                subThemeResource.addProperty(prefLabelProperty, prefLabel);
                subThemeResource.addProperty(typeProperty, conceptTypeResource);
            }
            mainThemeResource.addProperty(narrowerProperty, subThemeResource);
        }
    }

    /**
     * To be used in ShowLifter to query with a theme string and return it as a resource
     *
     * @param word the theme to get
     * @return the theme resource
     */
    public Resource getWordResource(String word){
        word = word.toLowerCase();

        Query query = QueryBuilder.buildTaxonomyQuery(word);
        ResultSet resultSet = QueryExecutionFactory.create(query, model).execSelect();

        if (resultSet.hasNext()){
            return resultSet.next().getResource(QueryBuilder.uriVariableName);
        }

        return null;
    }

    public Model getModel() {
        if (model == null)
            initializeModel();
        return model;
    }

    /*
     * Getters
     */
    public Property getPrefLabelProperty() {
        return prefLabelProperty;
    }

    public Property getHasTopConceptProp() {
        return hasTopConceptProp;
    }

    public Property getNarrowerProperty() {
        return narrowerProperty;
    }
}
