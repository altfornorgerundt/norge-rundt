package no.uib.info310.norgerundt.utils;

/**
 * Constants for prefixes to avoid duplicating code
 */
public abstract class PrefixHolder {

    public static final String NRK_SEASON_BASE = "https://tv.nrk.no/program/Episodes/norge-rundt/";
    public static final String NRK_EPISODE_BASE = "https://tv.nrk.no/serie/norge-rundt/";

    public static final String LOCAL_BASE = "http://www.uib.no.info310.aei020obe004/";
    public static final String RDF_BASE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    public static final String RDFS_BASE = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String PROGRAM_ONTOLOGY_BASE = "http://purl.org/ontology/po/";
    public static final String FOAF_BASE = "http://xmlns.com/foaf/0.1/";
    public static final String SKOS_BASE = "http://www.w3.org/2004/02/skos/core#";
    public static final String SCHEMA_BASE = "http://schema.org/";

    public static final String WIKIDATA_PROPERTY_BASE = "http://www.wikidata.org/prop/direct/";
    public static final String WIKIDATA_ENTITY_BASE = "http://www.wikidata.org/entity/";

    public static final String WORDNET_BASE = "http://www.wordnet.dk/owl/instance/2009/03/instances/";
    public static final String WN20SCHEMA_BASE = "http://www.w3.org/2006/03/wn/wn20/schema/";

}
