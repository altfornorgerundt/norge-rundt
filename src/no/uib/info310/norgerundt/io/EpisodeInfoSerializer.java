package no.uib.info310.norgerundt.io;

import no.uib.info310.norgerundt.utils.DurationDescriptionTuple;
import no.uib.info310.norgerundt.utils.NRKscraper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to serialize info scraped from nrk.no so the
 * scraper only runs when the data is not already there
 */
public class EpisodeInfoSerializer {

    private static NRKscraper scraper = NRKscraper.getInstance();

    /**
     * Scrapes NRK for information about all the episodes in the list
     * and stores it by serializing the information
     *
     * @param episodeURLs A list of urls corresponding to some episodes
     */
    public static void serializeScrapedEpInfo(List<String> episodeURLs) {
        Map<String, Integer> episodeDurationMap = new HashMap<>();
        Map<String, String> episodeDescriptionMap = new HashMap<>();

        for (String episodeURL : episodeURLs) {
            DurationDescriptionTuple infoTuple = scraper.scrapeEpisodeInfo(episodeURL);
            episodeDurationMap.put(episodeURL, infoTuple.getDurationInSeconds());
            episodeDescriptionMap.put(episodeURL, infoTuple.getDescription());

            if (episodeDescriptionMap.size() % 100 == 0) {
                System.out.println(episodeDescriptionMap.size() + "/" + episodeURLs.size() + " episodes scraped");
            }
        }
        serializeInfoMaps(episodeDurationMap, episodeDescriptionMap);
    }

    /**
     * Scrapes NRK for info about the episode, adds it to the two maps and then serializes them
     * Does nothing if the scraper fails to find any info
     */
    public static void appendAndSerializeEpisodeInfo(
            String episodeUrl, Map<String, String> descriptionMap, Map<String, Integer> durationMap) {
        DurationDescriptionTuple episodeInfo = scraper.scrapeEpisodeInfo(episodeUrl);

        if (episodeInfo != null) {
            durationMap.put(episodeUrl, episodeInfo.getDurationInSeconds());
            descriptionMap.put(episodeUrl, episodeInfo.getDescription());

            serializeInfoMaps(durationMap, descriptionMap);
        }
    }

    /**
     * Serializes the info in the maps of description and duration
     *
     * @param episodeDurationMap the map of durations to serialize
     * @param episodeDescriptionMap the map of descriptions to serialize
     */
    private static void serializeInfoMaps(Map<String, Integer> episodeDurationMap, Map<String, String> episodeDescriptionMap) {
        try {
            ObjectOutputStream durationOutStream = new ObjectOutputStream(
                    new FileOutputStream(EpisodeInfoDeserializer.CACHE_EPISODE_DURATIONS_FILE));
            ObjectOutputStream descriptionOutStream = new ObjectOutputStream(
                    new FileOutputStream(EpisodeInfoDeserializer.CACHE_EPISODE_DESCRIPTIONS_FILE));
            durationOutStream.writeObject(episodeDurationMap);
            descriptionOutStream.writeObject(episodeDescriptionMap);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
