package no.uib.info310.norgerundt.io;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * Deserializes the files created by EpisodeInfoSerializer
 */
public class EpisodeInfoDeserializer {

    public static final String CACHE_EPISODE_DURATIONS_FILE = "cache/episodeDurations.file";
    public static final String CACHE_EPISODE_DESCRIPTIONS_FILE = "cache/episodeDescriptions.file";
    private Map<String, String> episodeDescriptionMap;
    private Map<String, Integer> episodeDurationMap;

    public EpisodeInfoDeserializer(List<String> episodeUrlList) {
        loadDurationMap(episodeUrlList);
    }

    /**
     * Deserializes the durations and descriptions of all episodes
     * parsed from the excel spreadsheet for use in lifting
     * @param episodeUrlList
     */
    @SuppressWarnings("unchecked")
    private void loadDurationMap(List<String> episodeUrlList) {
        File durationsFile = new File(CACHE_EPISODE_DURATIONS_FILE);
        File descriptionsFile = new File(CACHE_EPISODE_DESCRIPTIONS_FILE);

        if (!durationsFile.exists() || !descriptionsFile.exists()) {
            System.out.println("InfoDeserializer: File containing episode durations and descriptions not found\n" +
                    "Starting scraper...");
            EpisodeInfoSerializer.serializeScrapedEpInfo(episodeUrlList);
        }

        try {
            ObjectInputStream durationInStream = new ObjectInputStream(new FileInputStream(durationsFile));
            ObjectInputStream descriptionInStream = new ObjectInputStream(new FileInputStream(descriptionsFile));
            episodeDurationMap = (Map<String, Integer>) durationInStream.readObject();
            episodeDescriptionMap = (Map<String, String>) descriptionInStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Failed to deserialize episode info\n" +
                    "If the problem persists, try clearing the cache files");
            e.printStackTrace();
        }
    }


    /**
     * Returns the duration scraped from NRK's website for the
     * episode corresponding to the input url
     */
    public int getEpisodeDurationInSeconds(String episodeUrl) {
        return episodeDurationMap.get(episodeUrl);
    }

    /**
     * Returns the description scraped from NRK's website for
     * the episode corresponding to the input url
     */
    public String getEpisodeDescription(String episodeUrl) {
        return episodeDescriptionMap.get(episodeUrl);
    }
}
