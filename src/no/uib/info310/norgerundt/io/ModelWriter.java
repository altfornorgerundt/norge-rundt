package no.uib.info310.norgerundt.io;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFWriter;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class to write the Show model to a file.
 */
public class ModelWriter {

    //Constants for file location
    public static final String SHOW_GRAPH_OUTPUT_LOCATION = "lifted data/showGraph.ttl";
    public static final String TAXONOMY_OUTPUT_LOCATION = "lifted data/themeTaxonomy.ttl";

    /**
     * Prints the model to console.
     *
     * @param model the model to print
     */
    public static void printGraph(Model model) {
        model.write(System.out, "ttl");
    }

    /**
     * Writes the specified model to an RDF TURTLE file.
     *
     * @param model the model to write to file
     * @param outputLocation the location to save the file
     */
    public static void writeGraph(Model model, String outputLocation) {
        try {
            File outputFile = new File(outputLocation);
            if (!outputFile.getParentFile().exists())
                outputFile.getParentFile().mkdirs();

            FileOutputStream outputStream = new FileOutputStream(outputLocation);
            RDFDataMgr.write(outputStream, model, Lang.TURTLE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
