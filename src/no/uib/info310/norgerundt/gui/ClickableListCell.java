package no.uib.info310.norgerundt.gui;

import javafx.scene.control.ListCell;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;

/**
 * Cell factory responsible for hiding url of items from side list
 * and attaching a double-click listener to each item in the list
 */
public class ClickableListCell extends ListCell<UrlTitleTuple> {

    @Override
    protected void updateItem(UrlTitleTuple item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setOnMouseClicked(null);
        } else {
            setText(item.getData());
            setOnMouseClicked(event -> {
                if (event.getClickCount() > 1)
                    GUIController.getInstance().handleDoubleClick(item);
            });
        }
    }

}
