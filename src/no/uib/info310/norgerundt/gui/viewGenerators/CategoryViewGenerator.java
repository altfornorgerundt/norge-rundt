package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import no.uib.info310.norgerundt.gui.GUIController;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.util.Collections;
import java.util.Map;

public class CategoryViewGenerator implements ViewGenerator {
    private static CategoryViewGenerator ourInstance = new CategoryViewGenerator();

    public static CategoryViewGenerator getInstance() {
        return ourInstance;
    }

    private CategoryViewGenerator() {
    }

    @Override
    public Pane buildView(Resource categoryResource) {
        BorderPane mainPane = new BorderPane();
        FlowPane topPane = new FlowPane();

        TextArea infoArea = new TextArea();
        infoArea.setEditable(false);

        mainPane.setTop(topPane);
        mainPane.setCenter(infoArea);

        String themeLabel = categoryResource.getProperty(ShowLifter.getInstance().getPrefLabelProperty()).getLiteral().getString();

        initializeButtons(topPane);
        fillInfoArea(infoArea, themeLabel);

        return mainPane;
    }

    private void initializeButtons(FlowPane topPane) {
        Property prefLabelProperty = ShowLifter.getInstance().getPrefLabelProperty();
        Map<UrlTitleTuple, Resource> categoryLabelResourceMap = ModelQuerier.getInstance().getCategoriesFromGraph(prefLabelProperty);

        Button viewCategoriesButton = new Button("Vis alle kategorier");
        viewCategoriesButton.addEventHandler(ActionEvent.ACTION, event -> {
            GUIController.getInstance().populateListView(categoryLabelResourceMap, GUIController.FilterEnum.Kategori);
        });
        topPane.getChildren().add(viewCategoriesButton);
    }

    private void fillInfoArea(TextArea infoArea, String themeLabel) {
        infoArea.setText("Kategorinavn: " + themeLabel + "\n");
    }

    @Override
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource categoryResource) {
        return ModelQuerier.getInstance().getThemesFromCategory(categoryResource);
    }
}
