package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextFlow;
import no.uib.info310.norgerundt.gui.GUIController;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.UrlManager;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;

import java.util.Collections;
import java.util.Map;

public class MunicipalityViewGenerator implements ViewGenerator {
    private static MunicipalityViewGenerator ourInstance = new MunicipalityViewGenerator();

    public static MunicipalityViewGenerator getInstance() {
        return ourInstance;
    }

    private MunicipalityViewGenerator() {
    }

    @Override
    public Pane buildView(Resource municipalityRes) {
        Resource countyResource = ModelQuerier.getInstance().getParentCountyOfMunicipality(municipalityRes);

        BorderPane mainPane = new BorderPane();
        FlowPane topPane = new FlowPane();

        TextArea infoArea = new TextArea();
        infoArea.setEditable(false);

        mainPane.setTop(topPane);
        mainPane.setCenter(infoArea);

        String municipalityName = municipalityRes.getProperty(ShowLifter.getInstance().getLabelProperty()).getLiteral().getString();
        initializeButtons(topPane, municipalityName, municipalityRes, countyResource);
        fillInfoArea(infoArea, municipalityName, municipalityRes, countyResource);

        return mainPane;
    }

    private void initializeButtons(FlowPane topPane, String municipalityName, Resource municipalityRes, Resource countyResource) {
        ObservableList<Node> children = topPane.getChildren();
        Map<UrlTitleTuple, Resource> muniNameResourceMap = ModelQuerier.getInstance().getMunicipalitiesFromCounty(countyResource);

        Button viewCountyButton = new Button("Vis overordnet fylke");
        viewCountyButton.addEventHandler(ActionEvent.ACTION, event -> {
            GUIController.getInstance().populateListView(muniNameResourceMap, GUIController.FilterEnum.Kommune);
            GUIController.getInstance().generateItemView(countyResource, CountyViewGenerator.getInstance());
        });
        children.add(viewCountyButton);

        TextFlow textFlow = new TextFlow(new Hyperlink("Vis " + municipalityName + " på Wikidata"));
        textFlow.addEventHandler(ActionEvent.ACTION, event -> {
            UrlManager.openUrlInBrowser(municipalityRes.getURI());
        });
        children.add(textFlow);
    }

    private void fillInfoArea(TextArea infoArea, String municipalityName, Resource municipalityRes, Resource countyResource) {
        StringBuilder infoBuilder = new StringBuilder();

        infoBuilder.append("Navn på kommune: ").append(municipalityName).append(" kommune\n");

        Literal countyNameLiteral = countyResource.getProperty(ShowLifter.getInstance().getLabelProperty()).getLiteral();
        infoBuilder.append("Del av ").append(countyNameLiteral.getString()).append(" fylke\n");

        infoArea.setText(infoBuilder.toString());
    }

    @Override
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource municipalityRes) {
        return ModelQuerier.getInstance().getClipsFromMunicipality(municipalityRes);
    }
}
