package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextFlow;
import no.uib.info310.norgerundt.gui.GUIController;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.DateFormatConverter;
import no.uib.info310.norgerundt.utils.TimestampConverter;
import no.uib.info310.norgerundt.utils.UrlManager;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;

import java.util.Map;

public class EpisodeViewGenerator implements ViewGenerator {
    private static EpisodeViewGenerator ourInstance = new EpisodeViewGenerator();
    private ShowLifter showLifter;

    public static EpisodeViewGenerator getInstance() {
        return ourInstance;
    }

    private EpisodeViewGenerator() {
        showLifter = ShowLifter.getInstance();
    }

    @Override
    public Pane buildView(Resource episodeResource) {
        Resource seasonResource = findParentSeason(episodeResource);
        Resource hostResource = findEpisodeHost(episodeResource);

        BorderPane mainPane = new BorderPane();
        FlowPane topPane = new FlowPane();
        mainPane.setTop(topPane);

        TextArea infoArea = new TextArea();
        infoArea.setEditable(false);

        mainPane.setCenter(infoArea);

        initializeButtons(topPane, episodeResource, seasonResource, hostResource);
        fillInfoArea(infoArea, episodeResource);

        return mainPane;
    }

    @Override
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource episodeResource) {
        return ModelQuerier.getInstance().getClipsFromEpisode(episodeResource);
    }

    public Resource findParentSeason(Resource episodeResource) {
        return ModelQuerier.getInstance().getParentSeasonOfEpisode(episodeResource);
    }

    private Resource findEpisodeHost(Resource episodeResource) {
        if (episodeResource.hasProperty(showLifter.getHostedByProperty())) {
            return (Resource) episodeResource.getProperty(showLifter.getHostedByProperty()).getObject();
        }
        return null;
    }

    private void initializeButtons(FlowPane topPane, Resource episodeResource, Resource seasonResource, Resource hostResource) {
        ObservableList<Node> children = topPane.getChildren();
        Map<UrlTitleTuple, Resource> episodeDateResourceMap = ModelQuerier.getInstance().getEpisodesFromSeason(seasonResource);

        Button showSeasonButton = new Button("Andre episoder i sesongen");
        children.add(showSeasonButton);
        showSeasonButton.addEventHandler(ActionEvent.ACTION, event -> {
            GUIController.getInstance().generateItemView(seasonResource, SeasonViewGenerator.getInstance());
            GUIController.getInstance().populateListView(episodeDateResourceMap, GUIController.FilterEnum.EpisodeDato);
        });

        TextFlow hyperLinkFlow = new TextFlow(new Hyperlink("Vis episode i nettleseren"));
        hyperLinkFlow.addEventHandler(ActionEvent.ACTION, event ->
                UrlManager.openUrlInBrowser(episodeResource.getURI()));
        children.add(hyperLinkFlow);
    }

    private void fillInfoArea(TextArea infoArea, Resource episodeResource) {
        StringBuilder infoBuilder = new StringBuilder();

        if (episodeResource.hasProperty(showLifter.getShortSynopsisProperty())) {
            String description = episodeResource.getProperty(showLifter.getShortSynopsisProperty()).getObject().toString();
            infoBuilder.append("Episodebeskrivelse: ").append(description).append("\n");
        }

        if (episodeResource.hasProperty(showLifter.getDurationProperty())){
            Literal durationLiteral = episodeResource.getProperty(showLifter.getDurationProperty()).getLiteral();
            String duration = TimestampConverter.convertSecondsToTimestamp(durationLiteral.getInt());
            infoBuilder.append("Episodelengde: ").append(duration).append("\n");
        }

        if (episodeResource.hasProperty(showLifter.getDateAiredProperty())){
            Literal dateLiteral = episodeResource.getProperty(showLifter.getDateAiredProperty()).getLiteral();
            String date = DateFormatConverter.convertDate(dateLiteral.getString());
            infoBuilder.append("Først vist: ").append(date).append("\n");
        }

        if (episodeResource.hasProperty(showLifter.getEpisodeNumberProperty())) {
            Literal numLiteral = episodeResource.getProperty(showLifter.getEpisodeNumberProperty()).getLiteral();
            int episodeNumber = numLiteral.getInt();
            infoBuilder.append("Episodenummer i sesong: ").append(episodeNumber).append("\n");
        }

        infoArea.setText(infoBuilder.toString());
    }
}
