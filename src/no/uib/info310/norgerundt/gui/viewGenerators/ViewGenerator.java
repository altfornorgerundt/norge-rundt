package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.scene.layout.Pane;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Resource;

import java.util.Map;

/**
 * Abstraction for generators responsible for generating
 * appropriate views for different kinds of items, displaying
 * their various properties and ways of navigating between them
 */
public interface ViewGenerator {

    /**
     * Generates a Pane containing navigation buttons and information
     * about the item input
     * @param item the semantic resource to generate a view of
     */
    public Pane buildView(Resource item);

    /**
     * Creates a map of all labels for the items to display in the
     * list of items and their semantic resources
     * Typically these items are a subtype of the input item,
     * so a list of clips for an episode or a list of themes
     * for a category
     * @param item The item to generate content for
     */
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource item);
}
