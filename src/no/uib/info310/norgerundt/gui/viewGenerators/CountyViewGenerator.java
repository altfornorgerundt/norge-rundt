package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextFlow;
import no.uib.info310.norgerundt.gui.GUIController;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.TupleMapBuilder;
import no.uib.info310.norgerundt.utils.UrlManager;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Map;

public class CountyViewGenerator implements ViewGenerator {
    private static CountyViewGenerator ourInstance = new CountyViewGenerator();

    public static CountyViewGenerator getInstance() {
        return ourInstance;
    }

    private CountyViewGenerator() {
    }

    @Override
    public Pane buildView(Resource countyResource) {
        BorderPane mainPane = new BorderPane();
        FlowPane topPane = new FlowPane();

        TextArea infoArea = new TextArea();
        infoArea.setEditable(false);

        mainPane.setTop(topPane);
        mainPane.setCenter(infoArea);

        String countyName = countyResource.getProperty(ShowLifter.getInstance().getLabelProperty()).getLiteral().getString();
        initializeButtons(topPane, countyName, countyResource);
        fillInfoArea(infoArea, countyName, countyResource);

        return mainPane;
    }

    private void initializeButtons(FlowPane topPane, String countyName, Resource countyResource) {
        Property labelProperty = ShowLifter.getInstance().getLabelProperty();
        Map<UrlTitleTuple, Resource> countyLabelResourceMap = ModelQuerier.getInstance().getCountiesFromGraph(labelProperty);

        Button showCountiesButton = new Button("Vis liste over fylker");
        showCountiesButton.addEventHandler(ActionEvent.ACTION, event -> {
            GUIController.getInstance().populateListView(countyLabelResourceMap, GUIController.FilterEnum.Fylke);
        });
        topPane.getChildren().add(showCountiesButton);

        TextFlow textFlow = new TextFlow(new Hyperlink("Vis " + countyName + " fylke på Wikidata"));
        textFlow.addEventHandler(ActionEvent.ACTION, event -> UrlManager.openUrlInBrowser(countyResource.getURI()));
        topPane.getChildren().add(textFlow);
    }

    private void fillInfoArea(TextArea infoArea, String countyName, Resource countyResource) {
        StringBuilder infoBuilder = new StringBuilder();

        infoBuilder.append("Navn på fylke: ").append(countyName).append(" fylke\n");

        Literal countyNumLiteral = countyResource.getProperty(ShowLifter.getInstance().getCountyNumProperty()).getLiteral();
        infoBuilder.append("Norsk fylkesnummer: ").append(countyNumLiteral.getString().substring(3)).append("\n");

        infoArea.setText(infoBuilder.toString());
    }

    @Override
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource countyResource) {
        return ModelQuerier.getInstance().getMunicipalitiesFromCounty(countyResource);
    }
}
