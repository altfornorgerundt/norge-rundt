package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextFlow;
import no.uib.info310.norgerundt.gui.GUIController;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.DateFormatConverter;
import no.uib.info310.norgerundt.utils.UrlManager;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.util.Map;

public class SeasonViewGenerator implements ViewGenerator{

    private static SeasonViewGenerator instance;

    private SeasonViewGenerator(){}

    public static SeasonViewGenerator getInstance() {
        if (instance == null) {
            instance = new SeasonViewGenerator();
        }
        return instance;
    }

    @Override
    public Pane buildView(Resource seasonResource) {
        BorderPane mainPane = new BorderPane();
        FlowPane topPane = new FlowPane();
        mainPane.setTop(topPane);

        TextArea infoArea = new TextArea();
        infoArea.setEditable(false);

        mainPane.setCenter(infoArea);

        initializeButtons(topPane, seasonResource);
        fillInfoArea(infoArea, seasonResource);

        return mainPane;
    }

    private void initializeButtons(FlowPane topPane, Resource seasonResource) {
        Property titleProperty = ShowLifter.getInstance().getTitleProperty();
        Map<UrlTitleTuple, Resource> seasonLabelResourceMap = ModelQuerier.getInstance().getSeasonsFromGraph(titleProperty);

        Button showSeasonButton = new Button("Vis alle sesonger");
        showSeasonButton.addEventHandler(ActionEvent.ACTION, event -> {
            GUIController.getInstance().populateListView(seasonLabelResourceMap, GUIController.FilterEnum.Sesong);
        });

        TextFlow textFlow = new TextFlow(new Hyperlink("Vis sesongoversikt i nettleser"));
        textFlow.addEventHandler(ActionEvent.ACTION, event -> {
            UrlManager.openUrlInBrowser(seasonResource.getURI());
        });
        topPane.getChildren().addAll(showSeasonButton, textFlow);
    }

    private void fillInfoArea(TextArea infoArea, Resource seasonResource) {
        StringBuilder infoBuilder = new StringBuilder();

        Literal seasonNumLiteral = seasonResource.getProperty(ShowLifter.getInstance().getSeasonNumberProperty()).getLiteral();
        infoBuilder.append(seasonNumLiteral.getInt()).append(". sesong av Norge Rundt\n");

        addStartEndDates(seasonResource, infoBuilder);

        if (seasonResource.hasProperty(ShowLifter.getInstance().getNumEpisodesProperty())) {
            Literal numEpisodesLiteral = seasonResource.getProperty(ShowLifter.getInstance().getNumEpisodesProperty()).getLiteral();
            infoBuilder.append("Antall episoder i sesong: ").append(numEpisodesLiteral.getInt());
        } else {
            infoBuilder.append("Antall episoder i sesong ukjent\n");
        }

        infoArea.setText(infoBuilder.toString());
    }

    private void addStartEndDates(Resource seasonResource, StringBuilder infoBuilder) {
        if (seasonResource.hasProperty(ShowLifter.getInstance().getStartDateProperty())) {
            Literal startDateLiteral = seasonResource.getProperty(ShowLifter.getInstance().getStartDateProperty()).getLiteral();
            infoBuilder.append("Sesong startet ").append(DateFormatConverter.convertDate(startDateLiteral.getString())).append("\n");
        }

        if (seasonResource.hasProperty(ShowLifter.getInstance().getEndDateProperty())) {
            Literal endDateLiteral = seasonResource.getProperty(ShowLifter.getInstance().getEndDateProperty()).getLiteral();
            infoBuilder.append("Sesong avsluttet ").append(DateFormatConverter.convertDate(endDateLiteral.getString())).append("\n");
        } else {
            infoBuilder.append("Dato for sesongavslutning ukjent\n");
        }
    }

    @Override
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource seasonResource) {
        return ModelQuerier.getInstance().getEpisodesFromSeason(seasonResource);
    }
}
