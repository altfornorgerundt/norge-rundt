package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import no.uib.info310.norgerundt.gui.GUIController;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;

import java.util.Map;

public class ThemeViewGenerator implements ViewGenerator {
    private static ThemeViewGenerator ourInstance = new ThemeViewGenerator();

    public static ThemeViewGenerator getInstance() {
        return ourInstance;
    }

    private ThemeViewGenerator() {
    }

    @Override
    public Pane buildView(Resource themeResource) {
        BorderPane mainPane = new BorderPane();
        FlowPane topPane = new FlowPane();

        TextArea infoArea = new TextArea();
        infoArea.setEditable(false);

        mainPane.setTop(topPane);
        mainPane.setCenter(infoArea);

        String themeLabel = themeResource.getProperty(ShowLifter.getInstance().getPrefLabelProperty()).getLiteral().getString();

        Resource categoryResource = ModelQuerier.getInstance().getParentCategoryOfTheme(themeResource);
        initializeButtons(topPane, themeResource, categoryResource);
        fillInfoArea(infoArea, themeLabel, categoryResource);

        return mainPane;
    }

    private void initializeButtons(FlowPane topPane, Resource themeResource, Resource categoryResource) {
        Map<UrlTitleTuple, Resource> themeLabelResourceMap = ModelQuerier.getInstance().getThemesFromCategory(categoryResource);

        Button viewCategoryButton = new Button("Klikk for å vise overordnet kategori");
        viewCategoryButton.addEventHandler(ActionEvent.ACTION, event -> {
            GUIController.getInstance().populateListView(themeLabelResourceMap, GUIController.FilterEnum.Tema);
            GUIController.getInstance().generateItemView(categoryResource, CategoryViewGenerator.getInstance());
        });

        topPane.getChildren().add(viewCategoryButton);
    }

    private void fillInfoArea(TextArea infoArea, String themeLabel, Resource categoryResource) {
        StringBuilder infoBuilder = new StringBuilder();

        infoBuilder.append("Temalabel: ").append(themeLabel).append("\n");

        Literal catLabelLiteral = categoryResource.getProperty(ShowLifter.getInstance().getPrefLabelProperty()).getLiteral();
        infoBuilder.append("Tilhører kategori: ").append(catLabelLiteral.getString()).append("\n");

        infoArea.setText(infoBuilder.toString());
    }

    @Override
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource themeResource) {
        return ModelQuerier.getInstance().getClipsFromTheme(themeResource);
    }
}
