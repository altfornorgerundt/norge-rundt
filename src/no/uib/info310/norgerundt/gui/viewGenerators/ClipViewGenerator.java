package no.uib.info310.norgerundt.gui.viewGenerators;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.TextFlow;
import no.uib.info310.norgerundt.gui.GUIController;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.*;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ClipViewGenerator implements ViewGenerator {
    private static ClipViewGenerator ourInstance = new ClipViewGenerator();
    private ShowLifter showLifter;

    public static ClipViewGenerator getInstance() {
        return ourInstance;
    }


    private ClipViewGenerator() {
        showLifter = ShowLifter.getInstance();
    }

    @Override
    public Map<UrlTitleTuple, Resource> buildSideListContent(Resource item) {
        //Clips have no sub-divisions so there is nothing to show in the sidepanel
        return Collections.emptyMap();
    }

    @Override
    public Pane buildView(Resource clipResource) {
        Resource episodeResource = findParentEpisode(clipResource);
        Resource municipalityRes = null;
        if (clipResource.hasProperty(showLifter.getFilmedAtProperty())) {
            municipalityRes = (Resource) clipResource.getProperty(showLifter.getFilmedAtProperty()).getObject();
        }

        BorderPane mainPane = new BorderPane();
        FlowPane topPane = new FlowPane();
        mainPane.setTop(topPane);

        initializeButtons(clipResource, episodeResource, municipalityRes, topPane);

        TextArea infoArea = new TextArea();
        infoArea.setEditable(false);

        mainPane.setCenter(infoArea);

        fillInfoArea(clipResource, episodeResource, municipalityRes, infoArea);
        return mainPane;
    }

    private Resource findParentEpisode(Resource clipResource) {
        return ModelQuerier.getInstance().getParentEpisodeOfClip(clipResource);
    }

    private void initializeButtons(Resource clipResource, Resource episodeResource, Resource municipalityRes, FlowPane topPane) {
        ObservableList<Node> childNodes = topPane.getChildren();
        Map<UrlTitleTuple, Resource> clipTitleResourceMap = ModelQuerier.getInstance().getClipsFromEpisode(episodeResource);

        Button viewEpisodeButton = new Button("Andre klipp i episoden");
        viewEpisodeButton.addEventHandler(ActionEvent.ACTION, event -> {
                GUIController.getInstance().populateListView(clipTitleResourceMap, GUIController.FilterEnum.Klipptittel);
                GUIController.getInstance().generateItemView(episodeResource, EpisodeViewGenerator.getInstance());
                });
        childNodes.add(viewEpisodeButton);

        if (municipalityRes != null && municipalityRes.hasProperty(showLifter.getLabelProperty())) {
            Button viewMunicipalityButton = new Button("Andre klipp fra samme sted");
            viewMunicipalityButton.addEventHandler(ActionEvent.ACTION, event ->

                    GUIController.getInstance().generateItemView(municipalityRes, MunicipalityViewGenerator.getInstance()));
            childNodes.add(viewMunicipalityButton);
        }

        TextFlow hyperLinkFlow = new TextFlow(new Hyperlink("Vis klipp i nettleser"));
        hyperLinkFlow.addEventHandler(ActionEvent.ACTION, event ->
                UrlManager.openUrlInBrowser(clipResource.getURI()));
        childNodes.add(hyperLinkFlow);

    }

    private void fillInfoArea(Resource clipResource, Resource episodeResource, Resource municipalityRes, TextArea infoArea) {
        try {
            StringBuilder infoBuilder = new StringBuilder();
            infoBuilder.append("Tittel: ").append(getClipTitle(clipResource)).append("\n");
            Literal durationLiteral = (Literal) clipResource.getProperty(showLifter.getDurationProperty()).getObject();

            String duration = TimestampConverter.convertSecondsToTimestamp(durationLiteral.getInt());
            infoBuilder.append("Klipplengde: ").append(duration).append("\n");

            Literal positionLiteral = clipResource.getProperty(showLifter.getClipNumberProperty()).getLiteral();
            infoBuilder.append("Posisjon i episode: ").append(positionLiteral.getInt()).append(". klipp").append("\n");

            addLocationInfo(municipalityRes, infoBuilder);
            addHostInfo(episodeResource, infoBuilder);
            addDateAired(episodeResource, infoBuilder);
            addThemeInfo(clipResource, infoBuilder);

            infoArea.setText(infoBuilder.toString());

        } catch (Exception e) {
            infoArea.setText("En feil oppstod i visningen av den valgte gjenstanden");
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private String getClipTitle(Resource clipResource) {
        return clipResource.getProperty(showLifter.getTitleProperty()).getLiteral().getString();
    }


    private void addLocationInfo(Resource municipalityRes, StringBuilder infoBuilder) {
        if (municipalityRes != null) {
            if (municipalityRes.hasProperty(showLifter.getLabelProperty())) {
                String municipalityName = municipalityRes.getProperty(showLifter.getLabelProperty()).getLiteral().getString();
                infoBuilder.append("Filmet i ").append(municipalityName).append(" kommune\n");
                return;
            }
        }
        infoBuilder.append("Filmet på ukjent sted\n");
    }

    private void addHostInfo(Resource episodeResource, StringBuilder infoBuilder) {
        if (episodeResource.hasProperty(showLifter.getHostedByProperty())) {
            Resource hostResource = (Resource) episodeResource.getProperty(showLifter.getHostedByProperty()).getObject();
            Literal hostLiteral = (Literal) hostResource.getProperty(showLifter.getNameProperty()).getObject();
            infoBuilder.append("Programleder: ").append(hostLiteral.getString()).append("\n");
        } else
            infoBuilder.append("Programleder ukjent\n");
    }

    private void addDateAired(Resource episodeResource, StringBuilder infoBuilder) {
        if (episodeResource.hasProperty(ShowLifter.getInstance().getDateAiredProperty())){
            Literal dateLiteral = episodeResource.getProperty(ShowLifter.getInstance().getDateAiredProperty()).getLiteral();
            infoBuilder.append("Klipp først vist på TV ").append(DateFormatConverter.convertDate(dateLiteral.getString())).append("\n");
        } else
            infoBuilder.append("Dato for første gang klippet ble vist på TV ukjent");
    }

    private void addThemeInfo(Resource clipResource, StringBuilder infoBuilder) {
        if (clipResource.hasProperty(showLifter.getHasSubjectProperty())) {
            List<String> themeList = new ArrayList<>();

            StmtIterator iterator = clipResource.listProperties(showLifter.getHasSubjectProperty());
            iterator.forEachRemaining(statement -> {
                Resource themeResource = (Resource) statement.getObject();
                Property prefLabelProperty = TaxonomyPopulator.getInstance().getPrefLabelProperty();

                if (themeResource.hasProperty(prefLabelProperty)) {
                    Literal prefLabel = (Literal) themeResource.getProperty(prefLabelProperty).getObject();
                    String prefLabelString = prefLabel.getString();
                    themeList.add(Character.toUpperCase(prefLabelString.charAt(0)) + prefLabelString.substring(1));
                }
            });

            if (themeList.size() > 0) {
                infoBuilder.append("Temaer: ");
                infoBuilder.append(themeList.stream().collect(Collectors.joining(", ")));
                infoBuilder.append("\n");
                return;
            }
        }
        infoBuilder.append("Ingen temaer funnet");
        infoBuilder.append("\n");
    }

}
