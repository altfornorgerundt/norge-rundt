package no.uib.info310.norgerundt.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Class responsible for launching the actual GUI
 */
public class GUI extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gui.fxml"));
        loader.setController(GUIController.getInstance());

        Parent root = loader.load();
        Scene scene = new Scene(root, 1280, 700);
        primaryStage.setTitle("Semantisk browser for Norge Rundt");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
