package no.uib.info310.norgerundt.gui;

import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import no.uib.info310.norgerundt.gui.viewGenerators.*;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class GUIController {

    private static GUIController instance;

    private Map<UrlTitleTuple, Resource> clipTitleResourceMap;
    private Map<UrlTitleTuple, Resource> seasonTitleResourceMap;
    private Map<UrlTitleTuple, Resource> categoryLabelResourceMap = null;
    private Map<UrlTitleTuple, Resource> themeLabelResourceMap = null;
    private Map<UrlTitleTuple, Resource> municipalityNameResourceMap = null;
    private Map<UrlTitleTuple, Resource> countyNameResourceMap = null;

    private Map<UrlTitleTuple, Resource> searchSuggestionMap;
    private Map<UrlTitleTuple, Resource> itemListMap;

    private AutoCompletionBinding<UrlTitleTuple> searchSuggester;
    private InvalidationListener listFilterBoxListener;

    private ShowLifter showLifter;

    //Graphical elements initialized by fxml file
    @FXML
    private ComboBox<FilterEnum> listFilterBox;
    @FXML
    private ComboBox<FilterEnum> searchFilterBox;
    @FXML
    private TextField searchField;
    @FXML
    private TextArea infoArea;
    @FXML
    private TextArea helpTextArea;
    @FXML
    private Tab normalViewTab;
    @FXML
    private Pane normalViewPane;
    @FXML
    private ListView<UrlTitleTuple> listView;


    /**
     * Enum used to keep track of which filters to apply and
     * which ViewGenerator to use to generate views for items
     */
    public enum FilterEnum {
        Klipptittel(ClipViewGenerator.getInstance()),
        Tema(ThemeViewGenerator.getInstance()),
        Kategori(CategoryViewGenerator.getInstance()),
        Kommune(MunicipalityViewGenerator.getInstance()),
        Fylke(CountyViewGenerator.getInstance()),

        //Not used for search suggestions
        EpisodeDato(EpisodeViewGenerator.getInstance()),
        Sesong(SeasonViewGenerator.getInstance());

        private ViewGenerator viewGenerator;

        FilterEnum(ViewGenerator viewGenerator) {
            this.viewGenerator = viewGenerator;
        }

        public ViewGenerator getViewGenerator() {
            return viewGenerator;
        }
    }

    private GUIController() {
    }

    public static GUIController getInstance() {
        if (instance == null)
            instance = new GUIController();
        return instance;
    }

    @FXML
    public void initialize() {
        showLifter = ShowLifter.getInstance();

        initializeSearchSuggestions();
        initializeListeners();
        initializeComboboxes();
        initializeTextAreaText();
    }

    /**
     * Initializes all the item collections for use by the
     * searchField by querying the semantic dataset generated
     * by the rest of the application
     */
    private void initializeSearchSuggestions() {
        ModelQuerier querier = ModelQuerier.getInstance();

        Property titleProperty = showLifter.getTitleProperty();
        Property prefLabelProperty = showLifter.getPrefLabelProperty();
        Property labelProperty = showLifter.getLabelProperty();

        clipTitleResourceMap = querier.getClipsFromGraph(titleProperty);
        seasonTitleResourceMap = querier.getSeasonsFromGraph(titleProperty);
        countyNameResourceMap = querier.getCountiesFromGraph(labelProperty);
        themeLabelResourceMap = querier.getThemesFromGraph(prefLabelProperty);
        categoryLabelResourceMap = querier.getCategoriesFromGraph(prefLabelProperty);
        municipalityNameResourceMap = querier.getMunicipalitiesFromGraph(labelProperty);
    }

    /**
     * Sets up event listeners for the various interactable elements
     */
    private void initializeListeners() {
        searchSuggester = TextFields.bindAutoCompletion(searchField, clipTitleResourceMap.keySet());

        //Generates view for any item that is selected through the search field
        //and populates the side list with its sub-items if there are any
        searchField.setOnAction(event -> {
            TextField textField = (TextField) event.getSource();
            if (!textField.getText().isEmpty()) {
                FilterEnum filterEnum = searchFilterBox.getSelectionModel().getSelectedItem();
                ViewGenerator generator = filterEnum.getViewGenerator();
                UrlTitleTuple item = new UrlTitleTuple(textField.getText());
                Resource resource = searchSuggestionMap.get(item);
                if (resource != null) {
                    generateItemView(resource, generator);
                    populateListView(generator.buildSideListContent(resource), getSubItemFilter(filterEnum));
                }
            }
        });

        //Generates view for any item that is selected through the side list
        listView.getSelectionModel().selectedItemProperty().addListener(event -> {
            UrlTitleTuple selectedItem = listView.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                ViewGenerator generator = listFilterBox.getSelectionModel().getSelectedItem().getViewGenerator();
                generateItemView(itemListMap.get(selectedItem), generator);
            }
        });

        //Populates list of suggestions backing the searchField when the filter is changed
        searchFilterBox.getSelectionModel().selectedItemProperty().addListener(event -> {
            FilterEnum selectedFilter = searchFilterBox.getSelectionModel().getSelectedItem();
            searchSuggester.dispose();
            searchSuggestionMap = getSuggestionListFromFilter(selectedFilter);
            searchSuggester = TextFields.bindAutoCompletion(searchField, searchSuggestionMap.keySet());
        });

        //Populates side list with the type of items selected
        listFilterBoxListener = event -> {
            FilterEnum filter = listFilterBox.getSelectionModel().getSelectedItem();
            Map<UrlTitleTuple, Resource> elements = getSuggestionListFromFilter(filter);
            populateListView(elements, filter);
        };
        listFilterBox.getSelectionModel().selectedItemProperty().addListener(listFilterBoxListener);
    }

    /**
     * Helper method used to keep track of which filter and generator
     * to use when generating list and view of subItems
     *
     * @param filterEnum the type of item to get subtype for
     * @return the FilterEnum value corresponding to the input
     *         FilterEnum's sub-type
     */
    private FilterEnum getSubItemFilter(FilterEnum filterEnum) {
        switch (filterEnum) {
            case Klipptittel:
                return FilterEnum.Klipptittel;
            case EpisodeDato:
                return FilterEnum.Klipptittel;
            case Sesong:
                return FilterEnum.EpisodeDato;
            case Kommune:
                return FilterEnum.Klipptittel;
            case Fylke:
                return FilterEnum.Kommune;
            case Tema:
                return FilterEnum.Klipptittel;
            case Kategori:
                return FilterEnum.Tema;
            default:
                throw new RuntimeException("FilterEnum without subItemfilter " + filterEnum);
        }
    }

    /**
     * Determines which collection of items to load based on an item filter
     */
    private Map<UrlTitleTuple, Resource> getSuggestionListFromFilter(FilterEnum selectedFilter) {
        switch (selectedFilter) {
            case Klipptittel:
                return clipTitleResourceMap;
            case Tema:
                return themeLabelResourceMap;
            case Kategori:
                return categoryLabelResourceMap;
            case Kommune:
                return municipalityNameResourceMap;
            case Fylke:
                return countyNameResourceMap;
            case Sesong:
                return seasonTitleResourceMap;
        }
        throw new RuntimeException("Invalid filter in searchFilterBox");
    }

    /**
     * Populates comboboxes with filter values and sets their default filter
     */
    private void initializeComboboxes() {
        listFilterBox.getItems().addAll(FilterEnum.Kommune, FilterEnum.Fylke, FilterEnum.Tema, FilterEnum.Kategori, FilterEnum.Sesong);
        searchFilterBox.getItems().addAll(FilterEnum.Klipptittel, FilterEnum.Kommune, FilterEnum.Fylke, FilterEnum.Tema, FilterEnum.Kategori);
        listFilterBox.setValue(FilterEnum.Kategori);
        searchFilterBox.setValue(FilterEnum.Klipptittel);
    }

    /**
     * Populates TextAreas with text
     */
    private void initializeTextAreaText() {
        infoArea.setText("Information will be presented here as items are selected");
        helpTextArea.setText("Velkommen til Norge Rundt!\n\n" +
                "I søkefeltet på toppen kan du søke etter klipp. " +
                "For å velge hva du søker etter kan du endre filteret i boksen til høyre for søkefeltet.\n" +
                "Under søkefeltet kan du trykke på boksen for å velge en liste over kommuner, fylker, tema eller kategorier.\n" +
                "I listen til venstre kan du klikke på et element for å få opp mer informasjon, " +
                "eller dobbelklikke for å velge elementet. Husk å gå tilbake til Normal view hvis du vil prøve.\n" +
                "Når du er inne på et klipp vil du se informasjonen i et tekstfelt. " +
                "Over dette er det knapper for å vise flere klipp fra samme episode eller samme sted, " +
                "og en lenke til klippet på NRK nett-TV.\n" +
                "Fra oversikten over en episode kan du også gå videre for å se alle episodene i en sesong, " +
                "og videre til en liste over alle sesongene. Også disse har lenker til NRK nett-TV.\n" +
                "Man kan bare få alle klipp fra et sted derson vi har data om hvor klippet er filmet. " +
                "Knappen vil bare vises hvis vi har informasjon om hvor klippet er filmet.\n\n" +
                "Når det gjelder Oslo har det seg slik at det er både et fylke og en kommune. " +
                "Det viste seg å være problematisk, og tiden ble for knapp til å fikse det.\n" +
                "Det betyr at man ikke kan finne klipp fra Oslo basert på sted, " +
                "men om du velger Klipptittel i søkeboksen, og søker på Oslo vil det fortsatt dukke opp mange resultater.\n" +
                "Og fanen som heter RDF View er tom, men det er rett og slett fordi vi gikk tom for tid.");
    }

    /**
     * Generates a view for an item based on a ViewGenerator and the
     * semantic resource of the item
     */
    public void generateItemView(Resource selectedItem, ViewGenerator viewGenerator) {
        Pane itemView = viewGenerator.buildView(selectedItem);
        if (itemView != null)
            normalViewTab.setContent(itemView);
    }

    /**
     * Populates side list with a list of items, sorted by lexical value
     * and updates its filter to the type of items supplied
     * @param elements A map of items to render in list and their semantic resources
     * @param filter The FilterEnum corresponding to the type of items supplied
     */
    public void populateListView(Map<UrlTitleTuple, Resource> elements, FilterEnum filter) {
        itemListMap = elements;
        List<UrlTitleTuple> keyList = new ArrayList<>(elements.keySet());
        keyList.sort(Comparator.comparing(UrlTitleTuple::getData));
        listView.getSelectionModel().clearSelection();
        listView.setItems(FXCollections.observableList(keyList));
        listView.setCellFactory(param -> new ClickableListCell());
        updateListFilterSilently(filter);
    }

    /**
     * Updates the filter of the side list silently
     * without triggering a selection event
     * @param filterEnum The new filter to use
     */
    private void updateListFilterSilently(FilterEnum filterEnum) {
        listFilterBox.getSelectionModel().selectedItemProperty().removeListener(listFilterBoxListener);
        listFilterBox.getSelectionModel().select(filterEnum);
        listFilterBox.getSelectionModel().selectedItemProperty().addListener(listFilterBoxListener);
    }

    /**
     * Generates a view for the item in the side list the user double-clicked on,
     * as well as updating the side list with its sub-items and the sub-items'
     * corresponding item filter
     * @param selectedItem The item the user double-clicked
     */
    public void handleDoubleClick(UrlTitleTuple selectedItem) {
        FilterEnum filterEnum = listFilterBox.getSelectionModel().getSelectedItem();
        ViewGenerator viewGenerator = filterEnum.getViewGenerator();
        Resource itemResource = itemListMap.get(selectedItem);
        generateItemView(itemResource, viewGenerator);
        populateListView(viewGenerator.buildSideListContent(itemResource), getSubItemFilter(filterEnum));
    }
}
