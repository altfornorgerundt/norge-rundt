package no.uib.info310.norgerundt.data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class representing a single Clip in an episode of "Norge Rundt"
 * with each field representing a category in the data set.
 */
public class Clip {

    private int timestampInSeconds;
    private int durationInSeconds;
    private LocalDate dato;
    private String innslag_nettadresse;
    private String innslag_opptak_kommune;
    private String innslag_tittel;
    private String innslag_hovedtema;
    private List<String> undertema;
    private String innslag_spesielt;
    private String innslag_opplevelse;
    private String medvirkende_antrekk;
    private String medvirkende_relasjoner;
    private String medvirkende_antall_menn;
    private String medvirkende_antall_kvinner;
    private String medvirkende_hovedperson1_kjonn;
    private String medvirkende_hovedperson2_kjonn;
    private String medvirkende_hovedperson3_kjonn;
    private String medvirkende_hovedperson1_alder;
    private String medvirkende_hovedperson2_alder;
    private String medvirkende_hovedperson3_alder;

    public Clip() {
        undertema = new ArrayList<>();
    }

    /**
     * Prints each field in a clip, separated by " - ".
     * Mainly for debugging purposes.
     */
    public void printAllFields() {
        System.out.println(dato + " - " +
                innslag_nettadresse + " - " +
                innslag_opptak_kommune + " - " +
                innslag_tittel + " - " +
                innslag_hovedtema + " - " +
                undertema.stream().collect(Collectors.joining(",")) + " - " +
                innslag_spesielt + " - " +
                innslag_opplevelse + " - " +
                medvirkende_antrekk + " - " +
                medvirkende_relasjoner + " - " +
                medvirkende_antall_menn + " - " +
                medvirkende_antall_kvinner + " - " +
                medvirkende_hovedperson1_kjonn + " - " +
                medvirkende_hovedperson2_kjonn + " - " +
                medvirkende_hovedperson3_kjonn + " - " +
                medvirkende_hovedperson1_alder + " - " +
                medvirkende_hovedperson2_alder + " - " +
                medvirkende_hovedperson3_alder
        );
    }

    /**
     * Adds a theme to the list of subthemes
     *
     * @param theme the theme to add to undertema
     */
    public void addSubTheme(String theme) {
        undertema.add(theme);
    }

    /*
     * Setters and getters beyond this point
     */

    public LocalDate getDato() {
        return dato;
    }

    public void setDato(LocalDate dato) {
        this.dato = dato;
    }

    public String getInnslag_nettadresse() {
        return innslag_nettadresse;
    }

    public void setInnslag_nettadresse(String innslag_nettadresse) {
        this.innslag_nettadresse = innslag_nettadresse;
    }

    public String getInnslag_opptak_kommune() {
        return innslag_opptak_kommune;
    }

    public void setInnslag_opptak_kommune(String innslag_opptak_kommune) {
        this.innslag_opptak_kommune = innslag_opptak_kommune;
    }

    public String getInnslag_tittel() {
        return innslag_tittel;
    }

    public void setInnslag_tittel(String innslag_tittel) {
        this.innslag_tittel = innslag_tittel;
    }

    public String getInnslag_hovedtema() {
        return innslag_hovedtema;
    }

    public void setInnslag_hovedtema(String innslag_hovedtema) {
        this.innslag_hovedtema = innslag_hovedtema;
    }

    public String getInnslag_spesielt() {
        return innslag_spesielt;
    }

    public void setInnslag_spesielt(String innslag_spesielt) {
        this.innslag_spesielt = innslag_spesielt;
    }

    public String getInnslag_opplevelse() {
        return innslag_opplevelse;
    }

    public void setInnslag_opplevelse(String innslag_opplevelse) {
        this.innslag_opplevelse = innslag_opplevelse;
    }

    public String getMedvirkende_antrekk() {
        return medvirkende_antrekk;
    }

    public void setMedvirkende_antrekk(String medvirkende_antrekk) {
        this.medvirkende_antrekk = medvirkende_antrekk;
    }

    public String getMedvirkende_relasjoner() {
        return medvirkende_relasjoner;
    }

    public void setMedvirkende_relasjoner(String medvirkende_relasjoner) {
        this.medvirkende_relasjoner = medvirkende_relasjoner;
    }

    public String getMedvirkende_antall_menn() {
        return medvirkende_antall_menn;
    }

    public void setMedvirkende_antall_menn(String medvirkende_antall_menn) {
        this.medvirkende_antall_menn = medvirkende_antall_menn;
    }

    public String getMedvirkende_antall_kvinner() {
        return medvirkende_antall_kvinner;
    }

    public void setMedvirkende_antall_kvinner(String medvirkende_antall_kvinner) {
        this.medvirkende_antall_kvinner = medvirkende_antall_kvinner;
    }

    public String getMedvirkende_hovedperson1_kjonn() {
        return medvirkende_hovedperson1_kjonn;
    }

    public void setMedvirkende_hovedperson1_kjonn(String medvirkende_hovedperson1_kjonn) {
        this.medvirkende_hovedperson1_kjonn = medvirkende_hovedperson1_kjonn;
    }

    public String getMedvirkende_hovedperson2_kjonn() {
        return medvirkende_hovedperson2_kjonn;
    }

    public void setMedvirkende_hovedperson2_kjonn(String medvirkende_hovedperson2_kjonn) {
        this.medvirkende_hovedperson2_kjonn = medvirkende_hovedperson2_kjonn;
    }

    public String getMedvirkende_hovedperson3_kjonn() {
        return medvirkende_hovedperson3_kjonn;
    }

    public void setMedvirkende_hovedperson3_kjonn(String medvirkende_hovedperson3_kjonn) {
        this.medvirkende_hovedperson3_kjonn = medvirkende_hovedperson3_kjonn;
    }

    public String getMedvirkende_hovedperson1_alder() {
        return medvirkende_hovedperson1_alder;
    }

    public void setMedvirkende_hovedperson1_alder(String medvirkende_hovedperson1_alder) {
        this.medvirkende_hovedperson1_alder = medvirkende_hovedperson1_alder;
    }

    public String getMedvirkende_hovedperson2_alder() {
        return medvirkende_hovedperson2_alder;
    }

    public void setMedvirkende_hovedperson2_alder(String medvirkende_hovedperson2_alder) {
        this.medvirkende_hovedperson2_alder = medvirkende_hovedperson2_alder;
    }

    public String getMedvirkende_hovedperson3_alder() {
        return medvirkende_hovedperson3_alder;
    }

    public void setMedvirkende_hovedperson3_alder(String medvirkende_hovedperson3_alder) {
        this.medvirkende_hovedperson3_alder = medvirkende_hovedperson3_alder;
    }

    public List<String> getUndertema() {
        return undertema;
    }

    public void setUndertema(List<String> undertema) {
        this.undertema = undertema;
    }

    public int getTimestampInSeconds() {
        return timestampInSeconds;
    }

    public void setTimestampInSeconds(int timestampInSeconds) {
        this.timestampInSeconds = timestampInSeconds;
    }

    public int getDurationInSeconds() {
        return durationInSeconds;
    }

    public void setDurationInSeconds(int durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }
}

