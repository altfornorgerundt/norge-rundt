package no.uib.info310.norgerundt.data;

import no.uib.info310.norgerundt.utils.SeasonURIs;

import java.util.ArrayList;

/**
 * Class representing a season of "Norge Rundt". Each year is a single season.
 */
public class Season {
	
	private int year;
	private ArrayList<Episode> episodeList = new ArrayList<Episode>();
    private String seasonURI;

	/**
	 * Constructor for Season
	 *
	 * @param year the year to set for the season
	 */
	public Season(int year){
		this.year = year;
		this.seasonURI = SeasonURIs.getSeasonURI(year);
	}

	/**
	 * Prints information about each season to console.
	 * Mainly used for debugging purposes.
	 */
	public void printSeasonInfo(){
		System.out.println("Season: " + year);
		System.out.println("Number of episodes: " + episodeList.size());
		episodeList.forEach(Episode::printEpisodeInfo);
	}

	/**
	 * Adds an episode to the season
	 *
	 * @param episode the episode to add to the episodeList in season
	 */
	public void addEpisode(Episode episode){
		episodeList.add(episode);
	}

	/*
	 * Setters and getters beyond this point
	 */
	public ArrayList<Episode> getEpisodeList(){
		return episodeList;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

    public String getURI() {
        return seasonURI;
    }
}
