package no.uib.info310.norgerundt.data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing an episode of "Norge Rundt".
 * Each episode consists of multiple Clip objects.
 **/
public class Episode {

	private String episodeURI;
	private LocalDate date;
	private List<Clip> clipList;
	private int durationInSeconds;
	private String description;

	/**
	 * Constructor for Episode.
	 *
	 * @param date the date to set for the episode
	 * @param episodeURI the URI to set for the episode
	 */
	public Episode(LocalDate date, String episodeURI){
		this.date = date;
		this.episodeURI = episodeURI;
	}
	
	/**
	 * Prints out the date of the episode and 
	 * the information about the clips
	 */
	public void printEpisodeInfo(){
		System.out.println("Date: " + date);
		clipList.forEach(Clip::printAllFields);
	}

	/*
	 * Setters and getters beyond this point
	 */
	public List<Clip> getClipList(){
		return clipList;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getEpisodeURI() {
		return episodeURI;
	}

	public void setEpisodeURI(String episodeURI) {
		this.episodeURI = episodeURI;
	}

	public void setClipList(List<Clip> clips){
		clipList = clips;
	}

	public void setDurationInSeconds(int durationInSeconds) {
		this.durationInSeconds = durationInSeconds;
	}

	public int getDuration() {
		return durationInSeconds;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
