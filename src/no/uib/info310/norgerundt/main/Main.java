package no.uib.info310.norgerundt.main;

import javafx.application.Application;
import javafx.stage.Stage;
import no.uib.info310.norgerundt.gui.GUI;
import no.uib.info310.norgerundt.parsing.Parser;
import no.uib.info310.norgerundt.semantics.ModelQuerier;
import no.uib.info310.norgerundt.semantics.ShowLifter;
import no.uib.info310.norgerundt.io.ModelWriter;
import no.uib.info310.norgerundt.utils.TaxonomyPopulator;

import java.io.*;

/**
 * Class to handle the program logic. Runs the
 * parser and lifters before starting the GUI
 */
public class Main extends Application {

    private static Parser parser;

    /**
     * Entry point for the application
     * Starts by parsing clips, episodes and seasons and
     * then lifts this data by combining it with various
     * other data sources and then presenting it in a GUI
     */
    public static void main(String[] args) throws IOException {
        parser = new Parser();

        TaxonomyPopulator populator = TaxonomyPopulator.getInstance();

        populator.writeTaxonomyTerms(parser.getParsedClipList());
        ModelWriter.writeGraph(populator.getModel(), ModelWriter.TAXONOMY_OUTPUT_LOCATION);

        ShowLifter.initialize();
        ShowLifter showLifter = ShowLifter.getInstance();
        showLifter.liftSeasons(parser.getParsedSeasonList());

        ModelWriter.writeGraph(showLifter.getModel(), ModelWriter.SHOW_GRAPH_OUTPUT_LOCATION);
        ModelQuerier.initialize(showLifter.getModel());

        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GUI gui = new GUI();
        gui.start(primaryStage);
    }
}
