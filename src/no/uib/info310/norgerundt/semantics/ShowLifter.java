package no.uib.info310.norgerundt.semantics;

import no.uib.info310.norgerundt.data.Clip;
import no.uib.info310.norgerundt.data.Episode;
import no.uib.info310.norgerundt.data.Season;
import no.uib.info310.norgerundt.utils.ClipDateComparator;
import no.uib.info310.norgerundt.utils.ProgramHostExtractor;
import no.uib.info310.norgerundt.utils.TaxonomyPopulator;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.tdb.TDBFactory;

import java.time.LocalDate;
import java.util.*;

import static no.uib.info310.norgerundt.utils.PrefixHolder.*;

/**
 * Class to semantically lift the information about the show
 */
@SuppressWarnings("FieldCanBeLocal")
public class ShowLifter {

    private static final String NORGE_RUNDT_URI = "https://tv.nrk.no/serie/norge-rundt";
    private static final String NRK_WEB_SERVICE_URI = "https://tv.nrk.no/";
    private static final String NRK_BROADCASTER_URI = "https://www.nrk.no/";

    private static final String NORGE_RUNDT_DESCRIPTION = "Distriktenes magasin. Nære møter med folk og fe i hele norges land.";

    private Model model;

    private Resource norgeRundtResource;
    private Resource nrkWebServiceResource;
    private Resource nrkBroadcasterResource;

    private Resource clipTypeResource;
    private Resource episodeTypeResource;
    private Resource seasonTypeResource;

    private Property durationProperty;
    private Property typeProperty;
    private Property labelProperty;
    private Property hasClipProperty;
    private Property filmedAtProperty;
    private Property hasSubjectProperty;
    private Property seriesOfProperty;
    private Property episodeOfProperty;
    private Property shortSynopsisProperty;
    private Property locatedInProperty;
    private Property prefLabelProperty;
    private Property hostedByProperty;
    private Property nameProperty;
    private Property titleProperty;
    private Property dateAiredProperty;
    private Property endDateProperty;
    private Property startDateProperty;
    private Property numEpisodesProperty;
    private Property clipNumberProperty;
    private Property episodeNumberProperty;
    private Property seasonNumberProperty;
    private Property countyNumProperty;
    private Property municipalityNumProperty;

    private Model locationModel;
    private Model subjectTaxonomy;

    public static ShowLifter instance;
    private Resource agentTypeResource;

    /*
     * Singleton
     */
    public static ShowLifter getInstance() {
        if (instance != null)
            return instance;
        else
            throw new RuntimeException("ShowLifter must be initialized before usage");
    }

    public static void initialize() {
        instance = new ShowLifter();
        instance.initializeGraph();
    }

    /**
     * Constructor for the ShowLifter.
     */
    private ShowLifter() {
        Dataset dataset = TDBFactory.createDataset("cache/showCache");
        model = dataset.getDefaultModel();
        subjectTaxonomy = TaxonomyPopulator.getInstance().getModel();
        locationModel = LocationLifter.getInstance().getModel();
    }

    /**
     * Initializes the semantic graph. Starts the model, sets prefixes, creates resources.
     */
    public void initializeGraph() {
        model.begin();

        model.setNsPrefix("nrks", NRK_SEASON_BASE);
        model.setNsPrefix("nrke", NRK_EPISODE_BASE);
        model.setNsPrefix("po", PROGRAM_ONTOLOGY_BASE);
        model.setNsPrefix("wdt", WIKIDATA_PROPERTY_BASE);
        model.setNsPrefix("rdf", RDF_BASE);
        model.setNsPrefix("skos", SKOS_BASE);
        model.setNsPrefix("wn20", WN20SCHEMA_BASE);
        model.setNsPrefix("foaf", FOAF_BASE);
        model.setNsPrefix("local", LOCAL_BASE);
        model.setNsPrefix("wnet", WORDNET_BASE);

        model.add(subjectTaxonomy);
        model.add(locationModel);

        createProperties();

        clipTypeResource = model.createResource(PROGRAM_ONTOLOGY_BASE + "Clip");
        episodeTypeResource = model.createResource(PROGRAM_ONTOLOGY_BASE + "Episode");
        seasonTypeResource = model.createResource(PROGRAM_ONTOLOGY_BASE + "Series");
        agentTypeResource = model.createProperty(FOAF_BASE + "Agent");

        norgeRundtResource = model.createResource(NORGE_RUNDT_URI);
        nrkWebServiceResource = model.createResource(NRK_WEB_SERVICE_URI);
        nrkBroadcasterResource = model.createResource(NRK_BROADCASTER_URI);

        Property broadcastByProp = model.createProperty(PROGRAM_ONTOLOGY_BASE + "broadcaster");
        Property associatedWithServiceProp = model.createProperty(PROGRAM_ONTOLOGY_BASE + "service");

        nrkWebServiceResource.addProperty(broadcastByProp, nrkBroadcasterResource);
        norgeRundtResource.addProperty(associatedWithServiceProp, nrkWebServiceResource);
        norgeRundtResource.addProperty(shortSynopsisProperty, model.createLiteral(NORGE_RUNDT_DESCRIPTION));

        norgeRundtResource.addProperty(typeProperty, PROGRAM_ONTOLOGY_BASE + "Brand");
        nrkWebServiceResource.addProperty(typeProperty, PROGRAM_ONTOLOGY_BASE + "Service");
        nrkBroadcasterResource.addProperty(typeProperty, PROGRAM_ONTOLOGY_BASE + "Broadcaster");
    }

    /**
     * Creates all the necessary properties for lifting the show
     */
    private void createProperties() {
        //Shared properties
        typeProperty = model.createProperty(RDF_BASE + "type");
        labelProperty = model.createProperty(RDFS_BASE + "label");
        durationProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "duration");

        //Series-related properties
        seriesOfProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "series");
        numEpisodesProperty = model.createProperty(SCHEMA_BASE + "numberOfEpisodes");
        startDateProperty = model.createProperty(SCHEMA_BASE + "startDate");
        endDateProperty = model.createProperty(SCHEMA_BASE + "endDate");
        seasonNumberProperty = model.createProperty(SCHEMA_BASE + "seasonNumber");

        //Episode-related properties
        episodeOfProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "episode");
        shortSynopsisProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "short_synopsis");
        dateAiredProperty = model.createProperty(SCHEMA_BASE + "datePublished");
        hostedByProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "anchor");
        episodeNumberProperty = model.createProperty(SCHEMA_BASE + "episodeNumber");

        //Clip-related properties
        hasClipProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "clip");
        filmedAtProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "place");
        hasSubjectProperty = model.createProperty(PROGRAM_ONTOLOGY_BASE + "subject");
        titleProperty = model.createProperty(SCHEMA_BASE + "name");
        clipNumberProperty = model.createProperty(SCHEMA_BASE + "clipNumber");

        //Place-related properties
        locatedInProperty = model.createProperty(WIKIDATA_PROPERTY_BASE + "P150");
        countyNumProperty = model.createProperty(WIKIDATA_PROPERTY_BASE + "P300");
        municipalityNumProperty = model.createProperty(WIKIDATA_PROPERTY_BASE + "P2504");

        //Subject-related properties
        prefLabelProperty = model.createProperty(SKOS_BASE + "prefLabel");

        //Program host-related properties
        nameProperty = model.createProperty(FOAF_BASE + "name");
    }

    //TODO find out why xsd:date works, but not xsd:string or xsd:int

    /**
     * Semantically lifts every season up to 2016,
     * which is a special case since it is not complete
     * and handled separately.
     *
     * @param seasonList the list of seasons to lift
     */
    public void liftSeasons(List<Season> seasonList) {
        for (int index = 0; index < seasonList.size(); index++) {
            Season season = seasonList.get(index);
            Resource seasonResource = model.createResource(season.getURI());

            String seasonLabel = "Norge Rundt, sesong: " + season.getYear();
            seasonResource.addProperty(typeProperty, seasonTypeResource);
            seasonResource.addProperty(seasonNumberProperty, model.createTypedLiteral(index + 1, XSDDatatype.XSDint));
            seasonResource.addProperty(titleProperty, model.createTypedLiteral(seasonLabel, XSDDatatype.XSDstring));


            //Dataset only covers seasons up to 2016, so only lift number of episodes in
            //seasons that have finished
            if (season.getYear() < 2016) {
                Literal numEpisodes = model.createTypedLiteral(season.getEpisodeList().size(), XSDDatatype.XSDint);
                seasonResource.addProperty(numEpisodesProperty, numEpisodes);
            }

            norgeRundtResource.addProperty(seriesOfProperty, seasonResource);

            liftSeasonDates(season, seasonResource);
            liftEpisodes(season, seasonResource);
        }
        model.commit();
    }

    /**
     * Adds startDate and endDate to a season
     *
     * @param season         the season to get dates from
     * @param seasonResource the resource to add dates to
     */
    private void liftSeasonDates(Season season, Resource seasonResource) {
        List<Episode> episodes = season.getEpisodeList();
        episodes.sort(Comparator.comparing(Episode::getDate));

        LocalDate startDate = episodes.get(0).getDate();
        Literal startDateLiteral = model.createTypedLiteral(startDate.toString(), XSDDatatype.XSDdate);
        seasonResource.addProperty(startDateProperty, startDateLiteral);

        if (season.getYear() < 2016) {
            LocalDate endDate = episodes.get(episodes.size() - 1).getDate();
            Literal endDateLiteral = model.createTypedLiteral(endDate.toString(), XSDDatatype.XSDdate);
            seasonResource.addProperty(endDateProperty, endDateLiteral);
        }
    }

    /**
     * Semantically lifts the episodes in a season
     *
     * @param season         the season to get the episodes from
     * @param seasonResource the resource to add the semantic episodes to
     */
    public void liftEpisodes(Season season, Resource seasonResource) {
        ArrayList<Episode> episodeList = season.getEpisodeList();
        for (int index = 0; index < episodeList.size(); index++) {
            Episode episode = episodeList.get(index);
            Resource episodeResource = model.createResource(episode.getEpisodeURI());

            episodeResource.addProperty(typeProperty, episodeTypeResource);
            episodeResource.addProperty(episodeNumberProperty, model.createTypedLiteral(index + 1, XSDDatatype.XSDint));

            //Ignore default values for duration and description
            if (episode.getDuration() != -1) {
                episodeResource.addProperty(durationProperty, model.createTypedLiteral(episode.getDuration(), XSDDatatype.XSDint));
            }

            String episodeDescription = episode.getDescription();
            if (!episodeDescription.isEmpty()) {
                episodeResource.addProperty(shortSynopsisProperty, model.createTypedLiteral(episodeDescription, XSDDatatype.XSDstring));
                liftEpisodeHosts(episodeResource, episodeDescription);
            }

            Literal dateAired = model.createTypedLiteral(episode.getDate().toString(), XSDDatatype.XSDdate);
            episodeResource.addProperty(dateAiredProperty, dateAired);

            seasonResource.addProperty(episodeOfProperty, episodeResource);
            liftClips(episode, episodeResource);
        }
    }

    /**
     * Lifts episode hosts by calling liftEpisodeHost on each host in the list of hosts.
     *
     * @param episodeResource    the resource to add the hosts to
     * @param episodeDescription the description to get the hosts from
     */
    private void liftEpisodeHosts(Resource episodeResource, String episodeDescription) {
        List<String> hostNames = ProgramHostExtractor.getInstance().extractMultipleHosts(episodeDescription);
        if (hostNames != null) {
            for (String hostName : hostNames) {
                liftEpisodeHost(episodeResource, hostName);
            }
        } else {
            String hostName = ProgramHostExtractor.getInstance().extractHost(episodeDescription);
            if (hostName != null) {
                liftEpisodeHost(episodeResource, hostName);
            }
        }
    }

    /**
     * Lifts an episode host, to be used in liftEpisodeHosts
     *
     * @param episodeResource
     * @param hostName
     */
    private void liftEpisodeHost(Resource episodeResource, String hostName) {
        String programHostUrl = StringEscapeUtils.escapeHtml4(hostName);
        programHostUrl = programHostUrl.replaceAll(" ", "_");

        Resource programHost = model.createResource(LOCAL_BASE + programHostUrl);
        programHost.addProperty(typeProperty, agentTypeResource);
        programHost.addProperty(nameProperty, hostName);
        episodeResource.addProperty(hostedByProperty, programHost);
    }

    /**
     * Semantically lifts the clips in an episode
     *
     * @param parentEpisode   the episode to get clips from
     * @param episodeResource the episode resource to add clip resources to
     */
    public void liftClips(Episode parentEpisode, Resource episodeResource) {
        Map<Clip, Resource> clipResources = new HashMap<>();
        List<Clip> clipList = parentEpisode.getClipList();

        for (int index = 0; index < clipList.size(); index++) {
            Clip clip = clipList.get(index);
            Resource clipResource = model.createResource(clip.getInnslag_nettadresse());

            clipResource.addProperty(typeProperty, clipTypeResource);
            clipResource.addProperty(titleProperty, clip.getInnslag_tittel());
            clipResource.addProperty(clipNumberProperty, model.createTypedLiteral(index + 1, XSDDatatype.XSDint));
            episodeResource.addProperty(hasClipProperty, clipResource);

            liftClipSubjects(clip, clipResource);
            liftClipLocation(clip, clipResource);

            clipResources.put(clip, clipResource);
        }
        liftClipDurations(clipResources, parentEpisode.getClipList(), parentEpisode);
    }

    /**
     * Semantically lifts the subjects for a clip,
     * based on a taxonomy of subjects
     *
     * @param clip         the clip to get subjects from
     * @param clipResource the resource to add the clip subjects to
     */
    private void liftClipSubjects(Clip clip, Resource clipResource) {
        List<String> subjects = clip.getUndertema();

        for (String subject : subjects) {
            //As this resource also exists in our taxonomy
            Resource subjectResource = TaxonomyPopulator.getInstance().getWordResource(subject);
            if (subjectResource != null) {
                clipResource.addProperty(hasSubjectProperty, subjectResource);
            }
        }
    }

    /**
     * Semantically lifts the municipality where a clip was filmed by linking it to
     * data from WikiData, with support for clips being filmed in multiple municipalities
     */
    private void liftClipLocation(Clip clip, Resource clipResource) {
        for (String locationName : clip.getInnslag_opptak_kommune().split(",")) {
            Resource locationResource = LocationLifter.getInstance().getLocationResource(locationName.trim());

            if (locationResource != null) {
                clipResource.addProperty(filmedAtProperty, locationResource);
            }
        }
    }

    /**
     * Adds duration in seconds to clips based on the duration
     * of an episode and individual clips.
     *
     * @param clipResources the map of clip resources
     * @param clips         the list of clips
     * @param episode       the episode to get duration from
     */
    private void liftClipDurations(Map<Clip, Resource> clipResources, List<Clip> clips, Episode episode) {
        //Do nothing if parent episode has default duration
        if (episode.getDuration() != -1) {
            clips.sort(new ClipDateComparator());

            for (int index = 0; index < clips.size(); index++) {
                Clip clip = clips.get(index);
                Resource clipResource = clipResources.get(clip);

                //Special case to handle last clip in episodes
                if (index == clips.size() - 1) {
                    int duration = episode.getDuration() - clip.getTimestampInSeconds();
                    clipResource.addProperty(durationProperty, model.createTypedLiteral(duration, XSDDatatype.XSDint));
                } else {
                    int duration = clips.get(index + 1).getTimestampInSeconds() - clip.getTimestampInSeconds();
                    clipResource.addProperty(durationProperty, model.createTypedLiteral(duration, XSDDatatype.XSDint));
                }
            }
        }
    }

    /*
     * Getters beyond this point
     */
    public Model getModel() {
        return model;
    }

    public Property getDurationProperty() {
        return durationProperty;
    }

    public Property getTypeProperty() {
        return typeProperty;
    }

    public Property getHasClipProperty() {
        return hasClipProperty;
    }

    public Property getFilmedAtProperty() {
        return filmedAtProperty;
    }

    public Property getHasSubjectProperty() {
        return hasSubjectProperty;
    }

    public Property getSeriesOfProperty() {
        return seriesOfProperty;
    }

    public Property getEpisodeOfProperty() {
        return episodeOfProperty;
    }

    public Property getShortSynopsisProperty() {
        return shortSynopsisProperty;
    }

    public Property getLocatedInProperty() {
        return locatedInProperty;
    }

    public Property getPrefLabelProperty() {
        return prefLabelProperty;
    }

    public Property getHostedByProperty() {
        return hostedByProperty;
    }

    public Property getNameProperty() {
        return nameProperty;
    }

    public Property getTitleProperty() {
        return titleProperty;
    }

    public Property getDateAiredProperty() {
        return dateAiredProperty;
    }

    public Property getEndDateProperty() {
        return endDateProperty;
    }

    public Property getStartDateProperty() {
        return startDateProperty;
    }

    public Property getNumEpisodesProperty() {
        return numEpisodesProperty;
    }

    public Property getLabelProperty() {
        return labelProperty;
    }

    public Property getClipNumberProperty() {
        return clipNumberProperty;
    }

    public Property getEpisodeNumberProperty() {
        return episodeNumberProperty;
    }

    public Property getSeasonNumberProperty() {
        return seasonNumberProperty;
    }

    public Property getCountyNumProperty() {
        return countyNumProperty;
    }

    public Property getMunicipalityNumProperty() {
        return municipalityNumProperty;
    }
}
