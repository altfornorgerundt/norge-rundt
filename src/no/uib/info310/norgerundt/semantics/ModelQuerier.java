package no.uib.info310.norgerundt.semantics;

import no.uib.info310.norgerundt.utils.TupleMapBuilder;
import no.uib.info310.norgerundt.utils.UrlTitleTuple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelQuerier {

    private static ModelQuerier instance;
    private Model model;

    public static ModelQuerier getInstance() {
        if (instance == null) {
            throw new RuntimeException("ModelQuerier must be initialized before use");
        }
        return instance;
    }

    public static void initialize(Model model) {
        instance = new ModelQuerier(model);
    }

    private ModelQuerier(Model model) {
        this.model = model;
    }


    public Map<UrlTitleTuple, Resource> getClipsFromGraph(Property namingProperty) {
        Query query = QueryBuilder.buildClipListQuery();
        return getItemTupleMapFromGraph(query, namingProperty);
    }

    public Map<UrlTitleTuple, Resource> getSeasonsFromGraph(Property namingProperty) {
        Query query = QueryBuilder.buildSeasonListQuery();
        return getItemTupleMapFromGraph(query, namingProperty);
    }

    public Map<UrlTitleTuple, Resource> getThemesFromGraph(Property namingProperty) {
        Query query = QueryBuilder.buildThemeListQuery();
        return getItemTupleMapFromGraph(query, namingProperty);
    }

    public Map<UrlTitleTuple, Resource> getCategoriesFromGraph(Property namingProperty) {
        Query query = QueryBuilder.buildCategoryListQuery();
        return getItemTupleMapFromGraph(query, namingProperty);
    }

    public Map<UrlTitleTuple, Resource> getMunicipalitiesFromGraph(Property namingProperty) {
        Query query = QueryBuilder.buildMunicipalityListQuery();
        return getItemTupleMapFromGraph(query, namingProperty);
    }

    public Map<UrlTitleTuple, Resource> getCountiesFromGraph(Property namingProperty) {
        Query query = QueryBuilder.buildCountyListQuery();
        return getItemTupleMapFromGraph(query, namingProperty);
    }

    private Map<UrlTitleTuple, Resource> getItemTupleMapFromGraph(Query query, Property namingProperty) {
        List<Resource> itemResources = new ArrayList<>();

        ResultSet resultSet = QueryExecutionFactory.create(query, model).execSelect();

        while (resultSet.hasNext()) {
            itemResources.add(resultSet.next().getResource(QueryBuilder.uriVariableName));
        }
        return TupleMapBuilder.buildTupleResourceMap(itemResources, namingProperty);
    }

    public Resource getParentEpisodeOfClip(Resource clipResource) {
        Query query = QueryBuilder.buildEpisodeFromClipQuery(clipResource);
        return getItemFromGraph(query);
    }

    public Resource getParentSeasonOfEpisode(Resource episodeResource) {
        Query query = QueryBuilder.buildSeasonFromEpisodeQuery(episodeResource);
        return getItemFromGraph(query);
    }


    public Resource getParentCountyOfMunicipality(Resource municipalityRes) {
        Query query = QueryBuilder.buildCountyFromMunicipalityQuery(municipalityRes);
        return getItemFromGraph(query);
    }

    public Resource getParentCategoryOfTheme(Resource themeResource) {
        Query query = QueryBuilder.buildCategoryFromThemeQuery(themeResource);
        return getItemFromGraph(query);
    }

    public Resource getItemFromGraph(Query query) {
        ResultSet resultSet = QueryExecutionFactory.create(query, model).execSelect();

        if (resultSet.hasNext()) {
            return resultSet.next().getResource(QueryBuilder.uriVariableName);
        }
        return null;
    }

    public Map<UrlTitleTuple, Resource> getClipsFromEpisode(Resource episodeResource) {
        Map<UrlTitleTuple, Resource> clipTitles = new HashMap<>();

        StmtIterator iterator = episodeResource.listProperties(ShowLifter.getInstance().getHasClipProperty());
        iterator.forEachRemaining(statement -> {
            Resource clipRes = (Resource) statement.getObject();
            Literal titleLiteral = clipRes.getProperty(ShowLifter.getInstance().getTitleProperty()).getLiteral();
            UrlTitleTuple tuple = new UrlTitleTuple(clipRes.getURI(), titleLiteral.getString());
            clipTitles.put(tuple, clipRes);
        });

        return clipTitles;
    }

    public Map<UrlTitleTuple, Resource> getEpisodesFromSeason(Resource seasonResource) {
        Map<UrlTitleTuple, Resource> episodeDateResourceMap = new HashMap<>();

        StmtIterator iterator = seasonResource.listProperties(ShowLifter.getInstance().getEpisodeOfProperty());
        iterator.forEachRemaining(statement -> {
            Resource episodeRes = (Resource) statement.getObject();
            Literal dateLiteral = episodeRes.getProperty(ShowLifter.getInstance().getDateAiredProperty()).getLiteral();
            UrlTitleTuple tuple = new UrlTitleTuple(episodeRes.getURI(), dateLiteral.getString());
            episodeDateResourceMap.put(tuple, episodeRes);
        });

        return episodeDateResourceMap;
    }

    public Map<UrlTitleTuple, Resource> getClipsFromMunicipality(Resource municipalityRes) {
        Query query = QueryBuilder.buildClipsFromMunicipalityQuery(municipalityRes);
        Property titleProperty = ShowLifter.getInstance().getTitleProperty();
        return getSubItemMapFromItem(query, titleProperty);
    }

    public Map<UrlTitleTuple, Resource> getMunicipalitiesFromCounty(Resource countyResource) {
        Query query = QueryBuilder.buildMunicipalitiesFromCountyQuery(countyResource);
        Property labelProperty = ShowLifter.getInstance().getLabelProperty();
        return getSubItemMapFromItem(query, labelProperty);
    }

    public Map<UrlTitleTuple,Resource> getClipsFromTheme(Resource themeResource) {
        Query query = QueryBuilder.buildClipsFromThemeQuery(themeResource);
        Property titleProperty = ShowLifter.getInstance().getTitleProperty();
        return getSubItemMapFromItem(query, titleProperty);
    }

    public Map<UrlTitleTuple, Resource> getThemesFromCategory(Resource categoryResource) {
        Query query = QueryBuilder.buildThemesFromCategoryQuery(categoryResource);
        Property prefLabelProperty = ShowLifter.getInstance().getPrefLabelProperty();
        return getSubItemMapFromItem(query, prefLabelProperty);
    }

    private Map<UrlTitleTuple, Resource> getSubItemMapFromItem(Query query, Property itemLabelProperty){
        Map<UrlTitleTuple, Resource> subItemMap = new HashMap<>();

        ResultSet resultSet = QueryExecutionFactory.create(query, model).execSelect();

        resultSet.forEachRemaining(querySolution -> {
            Resource clipResource = querySolution.getResource(QueryBuilder.uriVariableName);
            Literal titleLiteral = clipResource.getProperty(itemLabelProperty).getLiteral();
            UrlTitleTuple urlTitleTuple = new UrlTitleTuple(clipResource.getURI(), titleLiteral.getString());
            subItemMap.put(urlTitleTuple, clipResource);
        });

        return subItemMap;
    }
}
