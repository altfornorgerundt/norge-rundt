package no.uib.info310.norgerundt.semantics;

import no.uib.info310.norgerundt.utils.PrefixHolder;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.tdb.TDBFactory;

import java.util.*;

/**
 * Class to query WikiData for norwegian municipalities and counties.
 */
@SuppressWarnings("SpellCheckingInspection")
public class LocationLifter {

    public static final String WIKIDATA_SPARQL_ENDPOINT = "http://query.wikidata.org/sparql";
    public static final int DEFAULT_QUERY_TIMEOUT = 5000;
    private static LocationLifter instance;

    private Model locationModel;

    // Blacklist for cases that break convention and must be handled manually
    private List<String> blacklist = Arrays.asList("Nes(Ak.)",
            "Sande (M.ogR.)",
            "Sande (Vestf.)",
            "Herøy (Nordl.)",
            "Nes(Busk.)",
            "Os (Hord.)",
            "Våler (Hedm.)",
            "Våler (Østf.)",
            "Bø (Nordl.)",
            "Os (Hedm.)",
            "Bø (Telem.)",
            "Herøy (M.ogR.)",
            "Oslo",
            "Oslokommune",
            "Kjøllefjord",
            "Jan Mayen",
            "Svalbard",
            "Nordsjøen",
            "Rødøy",
            "Bjørnøya",
            "Ukjent sted",
            "Utlandet");

    private List<String> specialCaseList = Arrays.asList("Nes(Ak.)",
            "Nes(Busk.)",
            "Sande (M.ogR.)",
            "Sande (Vestf.)",
            "Herøy (Nordl.)",
            "Herøy (M.ogR.)",
            "Os (Hedm.)",
            "Os (Hord.)",
            "Våler (Hedm.)",
            "Våler (Østf.)",
            "Bø (Nordl.)",
            "Bø (Telem.)");

    /**
     * Check if there is cached data,
     * fetch the data from WikiData and cache it if there isn't any.
     */
    private LocationLifter() {
        locationModel = TDBFactory.createDataset("cache/locationCache").getDefaultModel();
        if (locationModel.isEmpty()) {
            locationModel.add(fetchLocationModelFromWikidata());
            locationModel.commit();
        }
    }

    /*
     * Singleton
     */
    public static LocationLifter getInstance() {
        if (instance == null) {
            instance = new LocationLifter();
        }
        return instance;
    }

    /**
     * Query wikidata for counties and municipalities
     *
     * @return the resulting model
     */
    public Model fetchLocationModelFromWikidata() {
        Query countyQuery = QueryBuilder.buildMunicipalityCountyQuery();
        QueryExecution executor = QueryExecutionFactory.sparqlService(WIKIDATA_SPARQL_ENDPOINT, countyQuery);
        executor.setTimeout(DEFAULT_QUERY_TIMEOUT);

        Model result = executor.execConstruct();
        if (result.size() > 0) {
            return result;
        } else {
            throw new QueryException("Failed to query Wikidata");
        }
    }

    /**
     * Check if the input string is blacklisted,
     * if not return the resource
     *
     * @param municipalityName the input municipality name
     * @return the municipality name resource
     */
    public Resource getLocationResource(String municipalityName) {
        if (specialCaseList.contains(municipalityName)) {
            return handleSpecialCaseName(municipalityName);
        }
        //Dump basic empty Resource with WikiData URI for locations which aren't municipalities in Norway
        else if (blacklist.contains(municipalityName)) {
            return disambiguateName(municipalityName);
        }

        Query municipalityQuery = QueryBuilder.buildMunicipalityFromNameQuery(municipalityName);
        ResultSet result = QueryExecutionFactory.create(municipalityQuery, locationModel).execSelect();

        if (!result.hasNext()) {
            throw new QueryException("Can't find Resource for " + municipalityName + " municipality");
        }
        return result.next().getResource(QueryBuilder.municipalityVariableName);

    }

    /**
     * Handles special cases with ambiguous municipalitynames like Os (Hord.)
     * @param municipalityName The name of the municipality
     * @return A resource corresponding to the municipality
     */
    private Resource handleSpecialCaseName(String municipalityName) {
        Resource municipalityRes = disambiguateName(municipalityName);
        if (!locationModel.containsResource(municipalityRes)) {

            Query specialCaseQuery = QueryBuilder.buildSingleLocationWDQuery(municipalityRes);
            Model resultModel = QueryExecutionFactory.sparqlService(WIKIDATA_SPARQL_ENDPOINT, specialCaseQuery).execConstruct();

            if (resultModel.isEmpty())
                throw new QueryException("Can't find Resource for " + municipalityName + " municipality");
            else
                locationModel.add(resultModel);
        }

        return locationModel.getResource(municipalityRes.getURI());
    }

    /**
     * Check is a string is in the list of special cases,
     * return a basic blank resource with a WikiData URI if the string is found.
     *
     * @param municipalityName the string to test
     * @return a blank Resource with the correct WikiData URI if found
     */
    private Resource disambiguateName(String municipalityName) {
        String correctName;
        switch (municipalityName) {
            case "Kjøllefjord":
                correctName = "Lebesby";
                break;
            case "Svalbard":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q25231");
            case "Jan Mayen":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q14056");
            case "Rødøy":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q488046");
            case "Bjørnøya":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q194364");
            case "Nordsjøen":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q1693");
            case "Oslo":
            case "Oslokommune":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q585");
            case "Sande (M.ogR.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q501510");
            case "Sande (Vestf.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q183029");
            case "Herøy (Nordl.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q493932");
            case "Herøy (M.ogR.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q493762");
            case "Nes(Ak.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q488026");
            case "Nes(Busk.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q483986");
            case "Os (Hord.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q493936");
            case "Os (Hedm.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q507562");
            case "Våler (Hedm.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q501413");
            case "Våler (Østf.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q499446");
            case "Bø (Nordl.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q484012");
            case "Bø (Telem.)":
                return ResourceFactory.createResource(PrefixHolder.WIKIDATA_ENTITY_BASE + "Q499450");
            default:
                return null;
        }
        return getLocationResource(correctName);
    }

    /**
     * Gets the full Model complete with information about all counties
     * and municipalities, plus their relations lifted so far
     */
    public Model getModel() {
        return locationModel;
    }
}
