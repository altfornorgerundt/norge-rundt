package no.uib.info310.norgerundt.semantics;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Resource;

import static no.uib.info310.norgerundt.utils.PrefixHolder.*;

/**
 * Helper class containing static methods used to generate various Queries
 * used by the semantic lifters and queriers in the application
 */
public class QueryBuilder {

    public static String uriVariableName = "?uri";
    public static String municipalityVariableName = "?municipality";
    public static String lexicalFormVariableName = "?form";

    /**
     * Builds a query to query the WordNet cache for the URI of the input word
     */
    public static Query buildCacheQuery(String word) {
        return QueryFactory.create("PREFIX wn20schema: <" + WN20SCHEMA_BASE + "> " +
                "PREFIX rdf: <" + RDF_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " WHERE { " +
                uriVariableName + " wn20schema:lexicalForm \"" + word + "\"@nb }");
    }

    /**
     * Builds a query to construct a word resource based on WordNet
     * with lexical form and type
     */
    public static Query buildWordNetQuery(String word) {
        return QueryFactory.create("PREFIX wn20schema: <" + WN20SCHEMA_BASE + "> " +
                "PREFIX rdf: <" + RDF_BASE + ">" +
                "CONSTRUCT {" +
                "?uri rdf:type wn20schema:Word." +
                "?uri wn20schema:lexicalForm \"" + word + "\"@nb" +
                "} WHERE {" +
                "?uri wn20schema:lexicalForm \"" + word + "\"@nb }");
    }

    /**
     * Builds a query to find the lexical form of the word with
     * the input WordNet wordID
     */
    public static Query buildLexicalFormQuery(String wordID) {
        return QueryFactory.create("PREFIX wn20schema: <" + WN20SCHEMA_BASE + "> " +
                "PREFIX dn: <" + WORDNET_BASE + ">" +
                "SELECT " + lexicalFormVariableName + " WHERE {" +
                "dn:" + wordID + " wn20schema:lexicalForm " + lexicalFormVariableName +
                " } ");
    }

    /**
     * Builds a query which constructs a graph containing all the counties and
     * municipalities in Norway, with their IDs, relations and Norwegian labels
     */
    public static Query buildMunicipalityCountyQuery() {
        //wd:Q20 points to the country of Norway
        //Wikidata uses P31 instead of rdf:type, but they are equivalent through owl:equivalentProperty
        return QueryFactory.create("PREFIX wd: <" + WIKIDATA_ENTITY_BASE + ">" +
                "PREFIX wdt: <" + WIKIDATA_PROPERTY_BASE + ">" +
                "PREFIX rdfs: <" + RDFS_BASE + "> " +
                "PREFIX rdf: <" + RDF_BASE + "> " +
                "CONSTRUCT { " +
                "wd:Q20 wdt:P150 ?fylke . " +
                "?fylke wdt:P300 ?fylkeID . " +
                "?fylke wdt:P150 ?kommune . " +
                "?fylke rdfs:label ?fylkeNavn . " +
                "?fylke rdf:type ?fylkeType . " +
                "?kommune rdfs:label ?kommuneNavn . " +
                "?kommune wdt:P2504 ?kommuneID . " +
                "?kommune rdf:type ?kommuneType . " +
                " } WHERE { " +
                "wd:Q20 wdt:P150 ?fylke . " +
                "?fylke wdt:P300 ?fylkeID . " +
                "?fylke wdt:P150 ?kommune . " +
                "?fylke rdfs:label ?fylkeNavn . " +
                "?fylke wdt:P31 ?fylkeType . " +
                "?kommune rdfs:label ?kommuneNavn . " +
                "?kommune wdt:P2504 ?kommuneID . " +
                "?kommune wdt:P31 ?kommuneType . " +
                "FILTER langMatches(lang(?kommuneNavn), \"nb\") " +
                "FILTER langMatches(lang(?fylkeNavn), \"nb\") " +
                " }");
    }

    /**
     * Builds a query providing the same graph as buildMunicipalityCountyQuery()
     * but restricted to a single municipality, rather than all municipalities in
     * Norway
     */
    public static Query buildSingleLocationWDQuery(Resource municipalityResource) {
        return QueryFactory.create("PREFIX wd: <" + WIKIDATA_ENTITY_BASE + ">" +
                "PREFIX wdt: <" + WIKIDATA_PROPERTY_BASE + ">" +
                "PREFIX rdfs: <" + RDFS_BASE + "> " +
                "PREFIX rdf: <" + RDF_BASE + "> " +
                "CONSTRUCT { " +
                "wd:Q20 wdt:P150 ?fylke . " +
                "?fylke wdt:P300 ?fylkeID . " +
                "?fylke wdt:P150 wd:" + municipalityResource.getLocalName() + " . " +
                "?fylke rdfs:label ?fylkeNavn . " +
                "?fylke rdf:type ?fylkeType . " +
                "wd:" + municipalityResource.getLocalName() + " rdfs:label ?kommuneNavn . " +
                "wd:" + municipalityResource.getLocalName() + " wdt:P2504 ?kommuneID . " +
                "wd:" + municipalityResource.getLocalName() + " rdf:type ?kommuneType . " +
                " } WHERE { " +
                "wd:Q20 wdt:P150 ?fylke . " +
                "?fylke wdt:P300 ?fylkeID . " +
                "?fylke wdt:P150 wd:" + municipalityResource.getLocalName() + " . " +
                "?fylke rdfs:label ?fylkeNavn . " +
                "?fylke wdt:P31 ?fylkeType . " +
                "wd:" + municipalityResource.getLocalName() + " rdfs:label ?kommuneNavn . " +
                "wd:" + municipalityResource.getLocalName() + " wdt:P2504 ?kommuneID . " +
                "wd:" + municipalityResource.getLocalName() + " wdt:P31 ?kommuneType . " +
                "FILTER langMatches(lang(?kommuneNavn), \"nb\") " +
                "FILTER langMatches(lang(?fylkeNavn), \"nb\") " +
                " } ");
    }

    /**
     * Builds a query returning the URI of the municipality with the input name
     */
    public static Query buildMunicipalityFromNameQuery(String municipalityName) {
        return QueryFactory.create("PREFIX wd: <" + WIKIDATA_ENTITY_BASE + ">" +
                "PREFIX rdfs: <" + RDFS_BASE + "> " +
                "SELECT " + municipalityVariableName + " " +
                "WHERE { " +
                municipalityVariableName + " rdfs:label \"" + municipalityName + "\"@nb " +
                " }");
    }

    /**
     * Builds a query returning the URIs of all clips
     */
    public static Query buildClipListQuery() {
        return QueryFactory.create("PREFIX po: <" + PROGRAM_ONTOLOGY_BASE + ">" +
                "PREFIX rdf: <" + RDF_BASE + ">" +
                "SELECT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " rdf:type po:Clip . " +
                " }");
    }

    /**
     * Builds a query returning the URIs of all series
     */
    public static Query buildSeasonListQuery() {
        return QueryFactory.create("PREFIX po: <" + PROGRAM_ONTOLOGY_BASE + ">" +
                "PREFIX rdf: <" + RDF_BASE + ">" +
                "SELECT " + uriVariableName + " " +
                "WHERE {" +
                uriVariableName + " rdf:type po:Series . " +
                " }");
    }

    /**
     * Builds a query returning the URIs of all municipalities
     */
    public static Query buildMunicipalityListQuery() {
        return QueryFactory.create("PREFIX wd: <" + WIKIDATA_ENTITY_BASE + ">" +
                "PREFIX rdf: <" + RDF_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " rdf:type wd:Q755707 . " +
                " } ");
    }

    /**
     * Builds a query returning the URIs of all counties
     */
    public static Query buildCountyListQuery() {
        return QueryFactory.create("PREFIX wd: <" + WIKIDATA_ENTITY_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " a wd:Q192299 . " +
                " } ");
    }

    /**
     * Builds a query returning the URIs of all themes
     */
    public static Query buildThemeListQuery() {
        return QueryFactory.create("PREFIX skos: <" + SKOS_BASE + ">" +
                "PREFIX rdf: <" + RDF_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " rdf:type skos:Concept . " +
                "MINUS { " +
                uriVariableName + " skos:narrower ?x . " +
                " } " +
                " } ");
    }

    /**
     * Builds a query returning the URIs of all categories
     */
    public static Query buildCategoryListQuery() {
        return QueryFactory.create("PREFIX skos: <" + SKOS_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                "?schema skos:hasTopConcept " + uriVariableName + " . " +
                " }");
    }

    /**
     * Builds a query returning the URIs of all the clips
     * filmed in the input municipality
     *
     * @param municipalityResource a resource corresponding to the municipality
     */
    public static Query buildClipsFromMunicipalityQuery(Resource municipalityResource) {
        return QueryFactory.create("PREFIX po: <" + PROGRAM_ONTOLOGY_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " po:place <" + municipalityResource.getURI() + "> . " +
                " } ");
    }

    /**
     * Builds a query returning the URIs of all the
     * municipalities located in the input county
     *
     * @param countyResource a resource corresponding to the county
     */
    public static Query buildMunicipalitiesFromCountyQuery(Resource countyResource) {
        return QueryFactory.create("PREFIX rdfs: <" + RDFS_BASE + "> " +
                "PREFIX wdt: <" + WIKIDATA_PROPERTY_BASE + "> " +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                "<" + countyResource.getURI() + ">  wdt:P150 " + uriVariableName + " . " +
                " }");
    }

    /**
     * Builds a query returning the URI of the
     * parent episode of the input clip
     *
     * @param clipResource a resource corresponding to the clip
     */
    public static Query buildEpisodeFromClipQuery(Resource clipResource) {
        return QueryFactory.create("PREFIX po: <" + PROGRAM_ONTOLOGY_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " po:clip <" + clipResource.getURI() + "> ." +
                " }");
    }

    /**
     * Builds a query returning the URI of the
     * parent season of the input episode
     *
     * @param episodeResource a resource corresponding to the episode
     */
    public static Query buildSeasonFromEpisodeQuery(Resource episodeResource) {
        return QueryFactory.create("PREFIX po: <" + PROGRAM_ONTOLOGY_BASE + ">" +
                "SELECT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " po:episode <" + episodeResource.getURI() + "> ." +
                " }");
    }

    /**
     * Builds a query returning the URI of the
     * parent county of the input municipality
     *
     * @param municipalityRes a resource corresponding to the municipality
     */
    public static Query buildCountyFromMunicipalityQuery(Resource municipalityRes) {
        return QueryFactory.create("PREFIX wdt: <" + WIKIDATA_PROPERTY_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " wdt:P150 <" + municipalityRes.getURI() + "> . " +
                " } ");
    }

    /**
     * Builds a query returning the URI of the
     * parent category of the input theme
     *
     * @param themeResource a resource corresponding to the theme
     */
    public static Query buildCategoryFromThemeQuery(Resource themeResource) {
        return QueryFactory.create("PREFIX skos: <" + SKOS_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " skos:narrower + <" + themeResource.getURI() + "> . " +
                " } ");
    }

    /**
     * Builds a query returning a list of URIs for
     * all clips having the input theme
     *
     * @param themeResource a resource corresponding to the theme
     */
    public static Query buildClipsFromThemeQuery(Resource themeResource) {
        return QueryFactory.create("PREFIX po: <" + PROGRAM_ONTOLOGY_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " po:subject <" + themeResource.getURI() + "> . " +
                " } ");
    }

    /**
     * Builds a query returning a list of URIs for
     * all themes being part of the input category
     *
     * @param categoryResource a resource corresponding to the category
     */
    public static Query buildThemesFromCategoryQuery(Resource categoryResource) {
        return QueryFactory.create("PREFIX skos: <" + SKOS_BASE + ">" +
                "SELECT DISTINCT " + uriVariableName + " " +
                "WHERE { " +
                "<" + categoryResource.getURI() + "> skos:narrower " + uriVariableName + " . " +
                " } ");
    }

    /**
     * Builds a query returning a word or concept from the
     * application's taxonomy of clip themes and categories
     *
     * @param concept the concept or word to search for
     */
    public static Query buildTaxonomyQuery(String concept) {
        return QueryFactory.create("PREFIX skos: <" + SKOS_BASE + ">" +
                "SELECT " + uriVariableName + " " +
                "WHERE { " +
                uriVariableName + " skos:prefLabel \"" + concept + "\"@nb . " +
                " }");
    }
}
