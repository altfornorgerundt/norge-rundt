package no.uib.info310.norgerundt.semantics;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.tdb.TDBFactory;

public class SubjectLifter {

    public static final String PATH_TO_WORDNET_GRAPH = "res/words.rdf";
    public static final String SUBJECT_GRAPH_OUTPUT_LOCATION = "lifted data/subjectGraph.rdf";
    private static final String WNSCHEMA_BASE = "http://www.w3.org/2006/03/wn/wn20/schema/";
    private static final String DANNET_BASE = "http://www.wordnet.dk/owl/instance/2009/03/instances/";
    private static SubjectLifter instance;

    private Model subjectModel;
    private Model wordnetModel;

    private SubjectLifter() {
        Dataset subjectDataset = TDBFactory.createDataset("cache/wordCache");
        subjectModel = subjectDataset.getDefaultModel();
        instance = this;
    }

    /*
     * Singleton
     */
    public static SubjectLifter getInstance() {
        if (instance == null)
            instance = new SubjectLifter();
        return instance;
    }

    /**
     * Returns a model containing all the information found about this word in
     * either a local cache or Wordnet if not found in the cache
     */
    public Resource getWordResource(String word){
        return getWordResource(word, subjectModel);
    }

    /**
     * Returns a model containing all the information found about this word in
     * either a local cache or Wordnet if not found in the cache
     */
    public Resource getWordResource(String word, Model model) {
        model.begin();

        word = word.toLowerCase();
        Query query = QueryBuilder.buildCacheQuery(word);

        ResultSet resultSet = QueryExecutionFactory.create(query, model).execSelect();
        if (resultSet.hasNext()) {
            return resultSet.next().getResource(QueryBuilder.uriVariableName);
        } else {
            //Word isn't in the local cache, query wordnet model instead
            Model resultModel = getWordResourceFromWordnet(word);
            //If the word was found, add it to the cache
            if (!resultModel.isEmpty()) {
                model.add(resultModel);
                model.commit();
                return getWordResource(word, model);
            } else {
                return null;
            }
        }
    }

    /**
     * Queries wordnet graph looking for the specified word, returning a graph containing
     * all the information about the particular word in the form of a model, if found.
     */
    private Model getWordResourceFromWordnet(String word) {
        if (wordnetModel == null) {
            Dataset wordnetDataset = RDFDataMgr.loadDataset(PATH_TO_WORDNET_GRAPH);
            wordnetModel = wordnetDataset.getDefaultModel();
        }

        Query wordNetQuery = QueryBuilder.buildWordNetQuery(word);

        //Both returns the resulting model and adds it to the subject model
        return QueryExecutionFactory.create(wordNetQuery, wordnetModel).execConstruct();
    }

    /**
     * Gets the lexical form of a word from WordNet.
     *
     * @param word the word to get the lexical form of.
     * @return the lexical form of the work
     */
    public Literal getLexicalForm(Resource word) {
        String wordID = word.getLocalName();
        Query lexicalFormQuery = QueryBuilder.buildLexicalFormQuery(wordID);

        ResultSet resultSet = QueryExecutionFactory.create(lexicalFormQuery, subjectModel).execSelect();
        if (resultSet.hasNext()) {
            return resultSet.next().getLiteral(QueryBuilder.lexicalFormVariableName);
        }
        return null;
    }
}
