package no.uib.info310.norgerundt.parsing;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import no.uib.info310.norgerundt.data.Clip;
import no.uib.info310.norgerundt.io.EpisodeInfoDeserializer;
import no.uib.info310.norgerundt.utils.TimestampConverter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import no.uib.info310.norgerundt.data.Episode;
import no.uib.info310.norgerundt.data.Season;
import no.uib.info310.norgerundt.utils.UrlManager;

/**
 * A class for parsing data from an Excel file into Clip objects
 * and add them to a list for use in the rest of the program.
 */
public class Parser {

    //The location of the excel file to parse
    private static final String INPUT_FILE = "res/norge_rundt_statistiskkmoro_2016_edited.xlsx";

    private ArrayList<String> dataList = new ArrayList<String>();
    private ArrayList<Clip> parsedClipList = new ArrayList<Clip>();
    private ArrayList<Episode> parsedEpisodeList = new ArrayList<Episode>();
    private ArrayList<Season> parsedSeasonList = new ArrayList<Season>();
    /**
     * Constructor for the parser class. Calls the methods that do the parsing.
     *
     * @throws IOException
     */
    public Parser() throws IOException {
        readExcelFile();
        createParsedSegmentList();
        createParsedEpisodeList();
        createParsedSeasonList();
    }

    /**
     * Creates a list of seasons based on the year the episodes were aired in.
     * Seasons of Norge Rundt are based on the year the episodes were aired.
     */
    public void createParsedSeasonList() {
        Map<Integer, List<Episode>> yearEpisodeMap = parsedEpisodeList.stream()
                .collect(Collectors.groupingBy(episode -> episode.getDate().getYear()));

        for (Integer year : yearEpisodeMap.keySet()) {
            Season season = new Season(year);
            for (Episode episode : yearEpisodeMap.get(year)) {
                season.addEpisode(episode);
            }
            parsedSeasonList.add(season);
        }
        parsedSeasonList.sort(Comparator.comparing(Season::getYear));
    }

    /**
     * Creates a list of episodes base on the date clips were aired.
     * Clips with the same date are assumed to belong to the same episode.
     */
    public void createParsedEpisodeList() {
        Map<LocalDate, List<Clip>> dateSegmentMap = parsedClipList.stream()
                .collect(Collectors.groupingBy(Clip::getDato));

        List<String> episodeUrls = dateSegmentMap.values().stream()
                .map(list -> list.get(0))
                .map(Clip::getInnslag_nettadresse)
                .map(UrlManager::stripTimestamp)
                .collect(Collectors.toList());

        EpisodeInfoDeserializer infoDeserializer = new EpisodeInfoDeserializer(episodeUrls);

        for (LocalDate date : dateSegmentMap.keySet()) {
            String episodeUrl = UrlManager.stripTimestamp(dateSegmentMap.get(date).get(0).getInnslag_nettadresse());
            Episode episode = new Episode(date, episodeUrl);
            episode.setClipList(dateSegmentMap.get(date));

            episode.setDurationInSeconds(infoDeserializer.getEpisodeDurationInSeconds(episodeUrl));
            episode.setDescription(infoDeserializer.getEpisodeDescription(episodeUrl));

            parsedEpisodeList.add(episode);
        }

        parsedEpisodeList.sort(Comparator.comparing(Episode::getDate));
    }

    /**
     * Iterates over the elements in dataList, counting to 32 "columns" before starting
     * on a new "row". Once all 32 values are added to a Clip object,
     * it is added to parsedClipList, a new Clip object is made and it iterates
     * through the rest of the dataList.
     */
    public void createParsedSegmentList() {

        int columnNum = 1;
        Clip clip = new Clip();

        for (int cellIndex = 32; cellIndex < dataList.size(); cellIndex++) {
            // Disable IntelliJ formatter to prevent switch from getting messy
            // Must be enables in settings - editor - code style - formatter control
            // @formatter:off
            switch (columnNum) {
                case 1: clip = new Clip(); break;
                case 2:
                    String url = dataList.get(cellIndex);
                    if (url.isEmpty())
                        return;
                    url = UrlManager.removeAutostartFromURL(url);
                    url = UrlManager.stripDateFromUrl(url);
                    url = UrlManager.addTimestampToURL(url);
                    clip.setInnslag_nettadresse(url);
                    clip.setDato(UrlManager.getDateFromURL(dataList.get(cellIndex)));
                    clip.setTimestampInSeconds(TimestampConverter.convertTimestampToSeconds(dataList.get(cellIndex))); break;
                case 3: clip.setInnslag_opptak_kommune(dataList.get(cellIndex)); break;
                case 4: clip.setInnslag_tittel(dataList.get(cellIndex)); break;
                case 5: clip.setInnslag_hovedtema(dataList.get(cellIndex)); break;
                case 6: clip.setInnslag_spesielt(dataList.get(cellIndex)); break;
                case 7: clip.setInnslag_opplevelse(dataList.get(cellIndex)); break;
                case 8: clip.setMedvirkende_antrekk(dataList.get(cellIndex)); break;
                case 9: case 10: case 11: case 12: case 13: case 14:
                case 15: addSubThemes(clip, dataList.get(cellIndex)); break;
                case 16: clip.setMedvirkende_relasjoner(dataList.get(cellIndex)); break;
                case 17: case 18:
                case 19: addSubThemes(clip, dataList.get(cellIndex)); break;
                case 20: clip.setMedvirkende_antall_menn(dataList.get(cellIndex)); break;
                case 21: clip.setMedvirkende_antall_kvinner(dataList.get(cellIndex)); break;
                case 22: case 23: case 24:
                case 25: addSubThemes(clip, dataList.get(cellIndex)); break;
                case 26: clip.setMedvirkende_hovedperson1_kjonn(dataList.get(cellIndex)); break;
                case 27: clip.setMedvirkende_hovedperson2_kjonn(dataList.get(cellIndex)); break;
                case 28: clip.setMedvirkende_hovedperson3_kjonn(dataList.get(cellIndex)); break;
                case 29: clip.setMedvirkende_hovedperson1_alder(dataList.get(cellIndex)); break;
                case 30: clip.setMedvirkende_hovedperson2_alder(dataList.get(cellIndex)); break;
                case 31: clip.setMedvirkende_hovedperson3_alder(dataList.get(cellIndex)); break;
                case 32: addSubThemes(clip, dataList.get(cellIndex));
                         parsedClipList.add(clip);
                         columnNum = 0; break;
                default: throw new RuntimeException("Something that shouldn't happen, happened.");
            }
            //Re-enable formatter
            //@formatter:on
            columnNum++;
        }
    }

    /**
     * Adds a list of subThemes to the given Clip based on the string
     *
     * @param themeString A String containing subThemes separated by commas
     */
    public void addSubThemes(Clip clip, String themeString) {
        if (!themeString.isEmpty()) {
            List<String> themeList = Arrays.asList(themeString.split("(, ?)"));
            themeList.stream()
                    .map(theme -> theme.substring(0, 1).toUpperCase() + theme.substring(1))
                    .forEach(clip::addSubTheme);
        }
    }

    /**
     * Reads the specified Excel file (.xlsx), reads each cell value
     * and adds it to dataList. Empty cells are treated as null by default
     * in Excel and are replaced by a blank string to preserve the
     * "columns" as Excel is row based, and doesn't recognize columns.
     *
     * @throws IOException
     */
    public void readExcelFile() throws IOException {

        InputStream ExcelFileStream = new FileInputStream(INPUT_FILE);
        XSSFWorkbook workbook = new XSSFWorkbook(ExcelFileStream);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFRow row;
        XSSFCell cell;
        Iterator<Row> rowIterator = sheet.rowIterator();

        while (rowIterator.hasNext()) {
            row = (XSSFRow) rowIterator.next();

            for (int i = 0; i < 32; i++) {
                cell = row.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                dataList.add(cell.toString().trim());
            }
        }
        workbook.close();
    }

    /*
     * Setters and getters beyond this point
     */
    public ArrayList<Clip> getParsedClipList() {
        return parsedClipList;
    }

    public ArrayList<Episode> getParsedEpisodeList() {
        return parsedEpisodeList;
    }

    public ArrayList<Season> getParsedSeasonList() {
        return parsedSeasonList;
    }
}