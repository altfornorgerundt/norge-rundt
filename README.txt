Libraries used:
Java 8
Apache Jena 3.1.0
Apache POI 3.15
ControlsFX 8.20.9

Software used:
IntelliJ IDEA 2016.3
JavaFX Scene Builder 2.0
Atlassian SourceTree


Running the program:
Simply open the "Run NorgeRundt.jar" file in the root folder.
It will hopefully run as it should. Otherwise, opening the
project and starting the main method should do the trick.

First time startup will be slow as data is processed and caches are created.
Subsequent startups should be faster.


Warning:
There is a potential memory leak in the GUI. So far, it had only happened on one PC,
and still works fine on others. But since we have not been able to figure out what
caused it, and why it is only on one PC, we cannot guarantee that it will be stable.


Notes:
EpisodeDescription and EpisodeDuration cache files have their info scraped from nrk.no.
We keep them due to the amount of time it takes to scrape the data, well over 10 minutes.

The RDF View tab in the GUI is currently unimplemented, but was planned to be added if
there was time. This would have displayed the original RDF data in TURLTE format.

We developed in IntelliJ,  but have included Eclipse files,
so it should be posible to run in eclipse as well.

In case the application fails for some reason, we've included backup files of the
generated dataset in the "Lifted Data" folder.
